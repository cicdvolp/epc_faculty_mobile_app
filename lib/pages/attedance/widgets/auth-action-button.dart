import 'dart:convert';
import 'dart:io';


import 'package:epc_faculty/controller/apicall.dart';
import 'package:epc_faculty/controller/commonapicall.dart';
import 'package:epc_faculty/pages/attedance/db/database.dart';
import 'package:epc_faculty/pages/attedance/home/homescreen.dart';
import 'package:epc_faculty/pages/attedance/services/facenet.service.dart';
import 'package:epc_faculty/pages/attedance/view/Config/global.dart';
import 'package:epc_faculty/pages/css/common_ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';

// import 'package:imei_plugin/imei_plugin.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class User {
  String user;

  User({required this.user});

  static User fromDB(String dbuser) {
    return new User(user: dbuser.split(':')[0]);
  }
}

class AuthActionButton extends StatefulWidget {
  final msg;
  AuthActionButton(this._initializeControllerFuture,
      {required this.onPressed, required this.isLogin,this.msg,this.locationmasteid});
  final Future _initializeControllerFuture;
  final Function onPressed;
  final bool isLogin;
   var locationmasteid;
 
  @override
  _AuthActionButtonState createState() => _AuthActionButtonState();
}

class _AuthActionButtonState extends State<AuthActionButton> {
  /// service injection
  final FaceNetService _faceNetService = FaceNetService();
  final DataBaseService _dataBaseService = DataBaseService();

  bool fetchingdata = false;
  String _macaddr = "UNKNOWN";
  String _imeino = "UNKNOWN";
bool fetching1=false;
  String latituteData = avglat;
  String longitudeData = avglong;

  late User predictedUser;

  @override
  void initState() {
    super.initState();
   
      initPlatformState();
    
 
  }

  Future<void> initPlatformState() async {
    String macaddr="UNKNOWN";
    String imeino="UNKNOWN";
    try {
      macaddr = "yashmac";
      // imeino =
      //     await ImeiPlugin.getImei(shouldShowRequestPermissionRationale: false);
      imeino = "yashimei";
    } on PlatformException {
      macaddr = "Failed to get mac address";
    }
    if (!mounted) return;
    setState(() {
      _macaddr = macaddr;
      _imeino = imeino;
    });
  }

  void markattendanceData(BuildContext context) {
    var data;
   
    if (_macaddr != "UNKNOWN" &&
        _imeino != "UNKNOWN" &&
        latituteData != "UNKNOWN" &&
        longitudeData != "UNKNOWN") {
      var params = {
        "imeino": _imeino,
        "mac_address": _macaddr,
        "username": predicteduid,
        "latitude": avglat,
        "longitude": avglong,
      };
      
        fetchingdata = true;
      
      
      APICall.returnAPI("markattendanceapi", context, params)
          .then((apiHitData) {
      
     
        data = json.decode(apiHitData);
        
        if (data['status_code'].toString() == '200') {
          //     Navigator.pushNamed(
          //   context,
          //   "/Home",
          // );
      
            fetchingdata = false;
          toastFunctioncolor(context, data['message'], Colors.green);
          // Navigator.push(
          //   context,
          //   MaterialPageRoute(
          //     builder: (BuildContext context) => homescreen(
          //       msg: data['message'],
          //       error: false,
          //     ),
          //   ),
          // );
        } else {
        
            fetchingdata = false;
         
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => homescreen(
                msg: data['message'],
                error: true,
              ),
            ),
          );
        }
        // if (this.mounted) {
        //   setState(() {
        //     data = json.decode(apiHitData);
        //    
        //     if (data["status_code"] == '200') {
        //       toastFunctioncolor( context, data['message'], Colors.green);
        //    
        //       Navigator.pushNamed(
        //         context,
        //         "/Home"
        //       );
        //     } else {
        //       toastFunctioncolor(context, data['message'], Colors.red);
        //     }
        //   });
        // }
      });
    } else {
      toastFunctioncolor(
          context, "Your Loacation Not Turn On Can You Do It.", Colors.orange);
      if (widget.isLogin) {
        initPlatformState();
        // getCurrentLocation();
      }
    }
  }

  // Future<void> getCurrentLocation() async {
  //     final geoposition = await Geolocator.getCurrentPosition(desiredAccuracy:LocationAccuracy.best );
   
  //     latituteData = geoposition.latitude.toString();
  //     longitudeData =  geoposition.longitude.toString();
     
  
  // }
  
   Future<void> savedeviceinfo(BuildContext context) async {
     print("save deviceinfo");
     print(widget.locationmasteid);
    var data;
    // if (_macaddr != "UNKNOWN" && _imeino != "UNKNOWN") {
      var params = {
        "mobileno": "",
        "imeino": _imeino,
        "mac_address": _macaddr,
        "username": registrationuid,
        "mobilebiometriclocationid": widget.locationmasteid
      };
    
    
         
     
        // APICall.returnAPI("savedeviceinfo", context, params).then((apiHitData) {
    //       print(apiHitData);
    //       if (this.mounted) {
    //           print(apiHitData);
    //         setState(() {
    //           data = json.decode(apiHitData);
    //           print("json");
    //              print(data);
    //           if (data["status_code"] == '200') {
    //            print(data);
    //               fetching1 = false;
               
    //             // Navigator.push(
    //             //   context,
    //             //   MaterialPageRoute(
    //             //     builder: (BuildContext context) => potocapture(
    //             //       cameraDescription: widget.cameraDescription,
    //             //       masterid: localtionmasterid,
    //             //       msg: data['message'],
    //             //     ),
    //             //   ),
    //             // );
    //             _signUp(context,data['message']);
    //           } else {
              
    //               fetching1 = false;
              
    //             toastFunctioncolor(context, data['message'], Colors.red);
    //           }
    //         });
    //       }
    //     });
      
    // }
    
      SharedPreferences prefs = await SharedPreferences.getInstance();
    uid = prefs.getString("loginid")!;
    orgid = prefs.getString("orgid")!;
    instid = prefs.getString("instid")!;
      var headerValue = {
      "Accept": "application/json",
      //"EPS-token": accessToken,
      "EPS-loginid": uid,
      "router-path": "/app-link",
      "EPS-orgid": orgid,
      "EPS-instid": instid,
    };
   
    var response;
   try{
   var encoded= Uri.parse(MyApiCall.myApiCall['savedeviceinfo'].toString());
      response =
          await http.post(encoded, headers: headerValue, body: json.encode(params));
      var body = json.decode(response.body);
    
     if (body["status_code"] == '200') {
               print(body);
                  fetching1 = false;
               
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //     builder: (BuildContext context) => potocapture(
                //       cameraDescription: widget.cameraDescription,
                //       masterid: localtionmasterid,
                //       msg: data['message'],
                //     ),
                //   ),
                // );
            
   _signUp(context,body['message']);
            
             
              }
              else{
                  toastFunctioncolor(context, body['message'] , Colors.red);
              }

      // debugPrint(body, wrapWidth: 1024);

      final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
      pattern
          .allMatches(response.body.toString())
          .forEach((match) => print(match.group(0)));
   }
   catch(E){
       toastFunctioncolor(context,  E.toString(), Colors.red);
   }
    
  }

  Future _signUp(context,msg) async {
    print(msg);
    List predictedData = _faceNetService.predictedData;
    String user = registrationuid;

  

    await _dataBaseService.saveData(user, predictedData);

    this._faceNetService.setPredictedData(null);

    var appDir = (await getTemporaryDirectory()).path;
  
    Directory(appDir).delete(recursive: true);

    Navigator.pop(context, true);
    Navigator.pop(context, true);
    toastFunctioncolor(context,msg,Colors.green);
  }

  Future _signIn(context) async {
    var appDir = (await getTemporaryDirectory()).path;
  
    Directory(appDir).delete(recursive: true);

    predicteduid = predictedUser.user;

    //  Navigator.pushNamed(
    //                           context,
    //                          "/markattendance"
    //                         );
    markattendanceData(context);
  }

  String _predictUser() {
    String userAndPass = _faceNetService.predict();
  
    return userAndPass ;
  }

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton.extended(
      label: Text('Capture'),
      icon: Icon(Icons.camera_alt),
      
      onPressed: () async {
        try {
         
          await widget._initializeControllerFuture;
          
          bool faceDetected = await widget.onPressed();

          if (faceDetected) {
            if (widget.isLogin) {
              var userAndPass = _predictUser();
              if (userAndPass != null) {
                this.predictedUser = User.fromDB(userAndPass);
                setState(() {
                                   predicteduid = predictedUser.user;
                                });
                            
                 markattendanceData(context);
              }
            }
            
             if(!widget.isLogin)
             {
    Scaffold.of(context)
                .showBottomSheet((context) => signSheet(context));
             }
            
      
          }
        } catch (e) {
        
          print(e);
        }
      },
    );
  }

  signSheet(context) {
    return Container(
      height: 100,
      child: Column(
        children: [
          widget.isLogin && predictedUser != null
              ? Center(
                  child: Column(
                    children: [
                      Container(
                        child: Text(
                          predictedUser.user,
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          RaisedButton(
                            color: Colors.blue[900],
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                                side: BorderSide(color: Colors.white)),
                            child: Row(
                              children: [
                                Text(
                                  'Home',
                                  style: TextStyle(color: Colors.white),
                                ),
                                Icon(
                                  Icons.home,
                                  color: Colors.white,
                                ),
                              ],
                            ),
                            onPressed: () async {
                              Navigator.pushNamed(context, "/Home");
                            },
                          ),
                          fetchingdata
                              ? Center(
                                  child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Colors.orange),
                                ))
                              : RaisedButton(
                                  color: Colors.green[500],
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: BorderSide(color: Colors.white)),
                                  child: Text(
                                    'Mark Attendance',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  onPressed: ()  {
                                     _signIn(context);
                                  },
                                ),
                        ],
                      ),
                    ],
                  ),
                )
              : !widget.isLogin
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        RaisedButton(
                          color: Colors.blue[900],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.white)),
                          child: Row(
                            children: [
                              Text(
                                'Home',
                                style: TextStyle(color: Colors.white),
                              ),
                              Icon(
                                Icons.home,
                                color: Colors.white,
                              ),
                            ],
                          ),
                          onPressed: () async {
                            // Navigator.pushNamed(context, "/Registration");
                            Navigator.pop(context);
                            Navigator.pop(context);
                          },
                        ),
                        fetching1?
                        Center(child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation(Colors.orange),))
                        :
                        RaisedButton(
                          color: Colors.green[500],
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.white)),
                          child: Row(
                            children: [
                              Text(
                                'Register Your Face',
                                style: TextStyle(color: Colors.white),
                              ),
                              Icon(
                                Icons.face,
                                color: Colors.white,
                              )
                            ],
                          ),
                          onPressed: () async {
                         
                            // await _signUp(context);
        //                     setState(() {
        //    fetching1 = true;
        // });
                           await savedeviceinfo(context);
                           
                          },
                        ),
                      ],
                    )
                  : Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("User not Found"),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            RaisedButton(
                              color: Colors.blue[900],
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18.0),
                                  side: BorderSide(color: Colors.white)),
                              child: Row(
                                children: [
                                  Text(
                                    'Home',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  Icon(
                                    Icons.home,
                                    color: Colors.white,
                                  ),
                                ],
                              ),
                              onPressed: () async {
                                Navigator.pushNamed(context, "/Home");
                              },
                            ),
                          ],
                        ),
                      ],
                    )
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}

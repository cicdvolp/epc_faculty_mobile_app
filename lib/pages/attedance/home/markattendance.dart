import 'dart:convert';


import 'package:epc_faculty/controller/apicall.dart';
import 'package:epc_faculty/pages/attedance/view/Config/global.dart';
import 'package:epc_faculty/pages/css/common_ui.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:geolocator/geolocator.dart';

class Markattendance extends StatefulWidget {
  @override
  _MarkattendanceState createState() => _MarkattendanceState();
}

class _MarkattendanceState extends State<Markattendance> {
  
  String _macaddr = "UNKNOWN";
  String _imeino = "UNKNOWN";

  String latituteData = "UNKNOWN";
  String longitudeData = "UNKNOWN";
  @override
  void initState() {
    super.initState();
    initPlatformState();
    getCurrentLocation();
  }
  

  Future<void> initPlatformState() async {
    String macaddr="UNKNOWN";
    String imeino="UNKNOWN";
    try {
      macaddr = "yashmac";
      // imeino = await ImeiPlugin.getImei(shouldShowRequestPermissionRationale: false);
      imeino = "yashimei";
    } on PlatformException {
      macaddr = "Failed to get mac address";
    }
    if (!mounted) return;
    setState(() {
      _macaddr = macaddr;
      _imeino = imeino;
    });
  }

  Future<void> getCurrentLocation() async {
     final geoposition = await Geolocator.getCurrentPosition(desiredAccuracy:LocationAccuracy.best );
    setState(() {
      latituteData = '${geoposition.latitude}';
      longitudeData = '${geoposition.longitude}';
    });
  }

  Future<void> markattendanceData() async {
    var data;

    if (_macaddr != "UNKNOWN" && _imeino != "UNKNOWN" && latituteData != "UNKNOWN" && longitudeData != "UNKNOWN") {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String loginid = prefs.getString('loginid').toString();
      String orgid = prefs.getString('orgid').toString();
      var params = {
        "orgid": orgid,
        "loginid":loginid,
        "imeino": _imeino,
        "mac_address": _macaddr,
        "username": predicteduid,
        "latitude": latituteData,
        "longitude": longitudeData,
      };

      APICall.returnAPI("markattendanceapi", context, params).then((apiHitData) {
        if (this.mounted) {
          setState(() {
            data = json.decode(apiHitData);
            if (data["status_code"] == '200') {
              toastFunctioncolor( context, data['message'], Colors.green);
              Navigator.pushNamed(
                context,
                "/Home"
              );
            } else {
              toastFunctioncolor(context, data['message'], Colors.red);
            }
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Face Recognition"),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [Colors.blue.shade50, Colors.blue])),
          ),
      ),
      body: ListView(
        children: [
          Container(
            child: Card(
              margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
              child: Column(
                children: [
                  Container(
                    height: 70.0,
                    width: MediaQuery.of(context).size.width - 200,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 30.0),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.white)),
                        onPressed: () async {
                          markattendanceData();
                        },
                        color: Colors.blue,
                        child: Text(
                          "Capture & Mark",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

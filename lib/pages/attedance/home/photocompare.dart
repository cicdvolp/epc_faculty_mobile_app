// A screen that allows users to take a picture using a given camera.
import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:epc_faculty/controller/apicall.dart';
import 'package:epc_faculty/pages/attedance/home/homescreen.dart';
import 'package:epc_faculty/pages/attedance/services/camera.service.dart';
import 'package:epc_faculty/pages/attedance/services/facenet.service.dart';
import 'package:epc_faculty/pages/attedance/services/ml_vision_service.dart';
import 'package:epc_faculty/pages/attedance/view/Config/global.dart';
import 'package:epc_faculty/pages/attedance/widgets/FacePainter.dart';
import 'package:epc_faculty/pages/css/common_ui.dart';
import 'package:google_ml_vision/google_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:google_ml_vision/google_ml_vision.dart';
import 'package:path/path.dart' show join;
import 'dart:math' as math;
import 'package:path_provider/path_provider.dart';

// ignore: camel_case_types
class photocompare extends StatefulWidget {
  final CameraDescription cameraDescription;

  const photocompare({
    Key? key,
    required this.cameraDescription,
  }) : super(key: key);

  @override
  photocompareState createState() => photocompareState();
}

class User {
  String user;

  User({required this.user});

  static User fromDB(String dbuser) {
    return new User(user: dbuser.split(':')[0]);
  }
}

// ignore: camel_case_types
class photocompareState extends State<photocompare> {
  CameraService _cameraService = CameraService();
  MLVisionService _mlVisionService = MLVisionService();
  FaceNetService _faceNetService = FaceNetService();

  late Future _initializeControllerFuture;

  bool cameraInitializated = false;
  bool _detectingFaces = false;
  bool pictureTaked = false;

  bool _saving = false;
  bool _bottomSheetVisible = false;

  late String imagePath;
  late Size imageSize;
  late Face faceDetected;
  late User predictedUser;

  String _macaddr = "UNKNOWN";
  String _imeino = "UNKNOWN";

  String latituteData = avglat;
  String longitudeData = avglong;

  bool processing = false;
  bool imagesuccess = false;
  bool imagefail = false;
  @override
  void initState() {
    super.initState();

    _start();
  }

  @override
  void dispose() {
    _cameraService.dispose();
    super.dispose();
  }

  _start() async {
    setState(() {
      predicteduid = '';
    });
    _initializeControllerFuture =
        _cameraService.startService(widget.cameraDescription);
    await _initializeControllerFuture;

    setState(() {
      cameraInitializated = true;
    });

    _frameFaces();
  }

  /// draws rectangles when detects faces
  _frameFaces() {
    imageSize = _cameraService.getImageSize();

    _cameraService.cameraController.startImageStream((image) async {
      if (_cameraService.cameraController != null) {
        // if its currently busy, avoids overprocessing
        if (_detectingFaces) return;

        _detectingFaces = true;

        try {
          List<Face> faces = await _mlVisionService.getFacesFromImage(image);

          if (faces != null) {
            if (faces.length > 0) {
              // preprocessing the image
              setState(() {
                faceDetected = faces[0];
              });

              if (_saving) {
                _saving = false;
              
              }
  _faceNetService.setCurrentPrediction(image, faceDetected);
              // setState(() {
              //   processing = true;
              // });

              // await Future.delayed(Duration(seconds: 1));
              // onfacecapute();
              // await Future.delayed(Duration(seconds: 5));
              // setState(() {
              //   imagefail = false;
              //   imagesuccess = false;
              //   processing = false;
              // });
            } else {
              setState(() {
                faceDetected = [] as Face;
              });
            }
          }

          _detectingFaces = false;
        } catch (e) {
          _detectingFaces = false;
        }
      }
    });
  }

  onfacecapute() async {
    try {
      predicteduid = '';
      await _initializeControllerFuture;

      bool faceDetected = await onShot();

      if (faceDetected) {
        var userAndPass = _faceNetService.predict();
 print("object");
          print(userAndPass);
        if (userAndPass != null) {
          this.predictedUser = User.fromDB(userAndPass);
          setState(() {
            predicteduid = predictedUser.user;
          });
          print("object");
          print(predicteduid);
          if (predicteduid != '') {
            markattendanceData(context);
          }
        }
        if (predicteduid == '') {
          setState(() {
            imagefail = true;
          });
          toastFunctioncolor(context, "User not Found", Colors.red);
        }

        // toastFunctioncolor(context, "yash alekar", Colors.green);

      }
    } catch (e) {
      print(e);
    }
  }

  void markattendanceData(BuildContext context) {
    var data;

    if (device_Mac_address != "UNKNOWN" &&
        device_imei_no != "UNKNOWN" &&
        avglat != "UNKNOWN" &&
        avglong != "UNKNOWN") {
      var params = {
        "imeino": device_imei_no,
        "mac_address": device_Mac_address,
        "username": predicteduid,
        "latitude": avglat,
        "longitude": avglong,
      };

      APICall.returnAPI("markattendanceapi", context, params)
          .then((apiHitData) {
        data = json.decode(apiHitData);
print(data);
        if (data['status_code'].toString() == '200') {
          setState(() {
            imagesuccess = true;
            Intime=data['data']['intime']==null?"":data['data']['intime'];
            Outtime=data['data']['outtime']==null?"":data['data']['outtime'];

          });

          toastFunctioncolor(context, data['message'], Colors.green);
        } else {
          setState(() {
            imagefail = true;
          });

          toastFunctioncolor(context, data['message'], Colors.red);
        }
      });
    } else {
      toastFunctioncolor(
          context, "Your Loacation Not Turn On Can You Do It.", Colors.orange);
    }
  }

  /// handles the button pressed event
  Future<bool> onShot() async {
    if (faceDetected == null) {
      showDialog(
        builder: (BuildContext context) {
          return AlertDialog(
            content: Text('No face detected!'),
          );
        },
        context: context,
      );

      return false;
    } else {
      imagePath =
          join((await getTemporaryDirectory()).path, '${DateTime.now()}.png');

      _saving = true;

      // await Future.delayed(Duration(milliseconds: 500));
      // await _cameraService.cameraController.stopImageStream();
      // await Future.delayed(Duration(milliseconds: 200));

      //  imagePath= await _cameraService.takePicture();

      setState(() {
        // _bottomSheetVisible = true;
        // pictureTaked = true;
      });

      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    final double mirror = math.pi;
    final width = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: FutureBuilder<void>(
          future: _initializeControllerFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (pictureTaked) {
                return Container(
                  width: width,
                  child: Transform(
                      alignment: Alignment.center,
                      child: Image.file(File(imagePath)),
                      transform: Matrix4.rotationY(mirror)),
                );
              } else {
                return Transform.scale(
                  scale: 1.0,
                  child: AspectRatio(
                    aspectRatio: MediaQuery.of(context).size.aspectRatio,
                    child: OverflowBox(
                      alignment: Alignment.center,
                      child: FittedBox(
                        fit: BoxFit.fitHeight,
                        child: Container(
                          width: width,
                          height: MediaQuery.of(context).size.height,
                          child: processing && imagesuccess
                              ? Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width -
                                                100,
                                            height: 100.0,
                                            child: Image.asset(
                                                'assets/images/icons/yes.jpg')),
                                      ],
                                    ),
                                  ],
                                )
                              : processing && imagefail
                                  ? Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width -
                                                    100,
                                                height: 100.0,
                                                child: Image.asset(
                                                    'assets/images/icons/no.jpg')),
                                          ],
                                        ),
                                      ],
                                    )
                                  : processing
                                      ? Center(
                                          child: CircularProgressIndicator(
                                          valueColor:
                                              AlwaysStoppedAnimation<Color>(
                                                  Colors.orange),
                                        ))
                                      : Stack(
                                          fit: StackFit.expand,
                                          children: <Widget>[
                                            CameraPreview(_cameraService
                                                .cameraController),
                                            CustomPaint(
                                              painter: FacePainter(
                                                  face: faceDetected,
                                                  imageSize: imageSize),
                                            ),
                                            Column(
                                              children: [
                                                Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      horizontal: 30.0,
                                                      vertical: 50.0),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.end,
                                                    children: [
                                                      IconButton(
                                                        icon: Icon(
                                                          Icons.home,
                                                          color: Colors.blue,
                                                          size: 45.0,
                                                        ),
                                                        onPressed: () async {
                                                          // await Future.delayed(
                                                          //     Duration(
                                                          //         milliseconds:
                                                          //             500));
                                                          // await _cameraService
                                                          //     .cameraController
                                                          //     .stopImageStream();
                                                          // await Future.delayed(
                                                          //     Duration(
                                                          //         milliseconds:
                                                          //             200));
                                                          Navigator.pop(
                                                              context, true);
                                                        },
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                        ),
                      ),
                    ),
                  ),
                );
              }
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),

        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        // floatingActionButton: !_bottomSheetVisible
        //     ? AuthActionButton(
        //         _initializeControllerFuture,
        //         onPressed: onShot,
        //         isLogin: true,
        //       )
        //     : Container(),
        floatingActionButton: imagefail || imagesuccess || processing
            ? Container()
            : FloatingActionButton.extended(
                icon: Icon(Icons.camera_alt),
                onPressed: () async {
                  setState(() {
                    processing = true;
                  });

                  // await Future.delayed(Duration(seconds: 1));
                  onfacecapute();
                  await _cameraService
                      .cameraController
                      .stopImageStream();
                  await Future.delayed(Duration(seconds: 5));
                  setState(() {
                    imagefail = false;
                    imagesuccess = false;
                    processing = false;
    // Navigator.pop(
    // context, true);

                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (BuildContext context) => homescreen(
                          // msg: data['message'],
                          // error: false,
                        ),
                      ),
                    );

                  });

                },
                label: Text("capture")),
      ),
    );
  }
}

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:camera/camera.dart';
import 'package:epc_faculty/controller/apicall.dart';
import 'package:epc_faculty/pages/attedance/db/database.dart';
import 'package:epc_faculty/pages/attedance/home/avglocation.dart';
import 'package:epc_faculty/pages/attedance/home/photocompare.dart';
import 'package:epc_faculty/pages/attedance/home/verifyotp.dart';
import 'package:epc_faculty/pages/attedance/model/registerlocationmodel.dart';
import 'package:epc_faculty/pages/attedance/services/facenet.service.dart';
import 'package:epc_faculty/pages/attedance/services/ml_vision_service.dart';
import 'package:epc_faculty/pages/attedance/view/Config/global.dart';
import 'package:epc_faculty/pages/css/common_ui.dart';
import 'package:epc_faculty/pages/css/textcss.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:geolocator/geolocator.dart';
import 'package:percent_indicator/percent_indicator.dart';

// ignore: camel_case_types
class homescreen extends StatefulWidget {
  // homescreen({Key key}) : super(key: key);
  final String msg;
  final bool error;
  const homescreen({Key? key, this.msg="", this.error=false}) : super(key: key);
  @override
  _homescreenState createState() => _homescreenState();
}

// ignore: camel_case_types
class _homescreenState extends State<homescreen> {
  var percentprogress = 0.0;
  FaceNetService _faceNetService = FaceNetService();
  MLVisionService _mlVisionService = MLVisionService();
  DataBaseService _dataBaseService = DataBaseService();
  late CameraDescription cameraDescription  ;
  late StreamSubscription<Position> positionStream;
  bool showcontainer = false;
  bool loading=false;
  double maplat=0;
  double maplong=0;
  var register_location_name="";
  Set<Marker> _markers = {};
  late BitmapDescriptor pinLocationIcon;
  void login()  {
    setState(() {
      loginname = '';
      profile_photo = '';
    });

      var data;
var params={"":""};
print("--------------------------init attadance--------------------------------");
      APICall.returnAPI("init_attedance", context,params).then((apiHitData) {
        if (this.mounted) {
          setState(() {
            data = json.decode(apiHitData);
            print(data);
            if (data["status"] == '200') {
              setState(() {
                isregistration = data['isregistration'];
                isattendance = data['isattadance'];
                isregistrationlocation = data['isregistrationlocation'];
                //isregistrationlocation=true;
                loginname = data['inst_name'].toString();
                print(loginname);
                profile_photo = data['profile_photo'].toString();
                Intime=data['intime']==null?"":data['intime'];
                Outtime=data['outtime']==null?"":data['outtime'];
                compare_distance=data['compare_distance']==null?20:data['compare_distance'];
                no_of_geoloaction_compare=data['no_of_geolocation_fetch']==null?3.0:data['no_of_geolocation_fetch'];
              });
              // prefs.setString("loginid", data['loginid'].toString());
              // prefs.setString("orgid", data['orgid'].toString());
              // prefs.setString("instid", data['instid'].toString());
              // Navigator.pushNamed(context, "/Home");
            } else {
              toastFunctioncolor(context, data['msg'], Colors.red);
            }
          });
        }
      });

  }
  void initState() {
    super.initState();

    if (widget.msg != '' && widget.msg != null) {
      Future.delayed(Duration(seconds: 2), () {
        if (widget.error)
          toastFunctioncolor(context, widget.msg, Colors.red);
        else
          toastFunctioncolor(context, widget.msg, Colors.green);
      });
    }
    // addexp();
    setState(() {
      loading=true;
    });
     _setLoading(true);
    login();
     setCustomMapPin();
     getCurrentLocation();
    _startUp();
    getallregisterlocation();
     _setLoading(false);
      setState(() {
     loading=false;
    });
  }
   getCurrentLocation() async {
     LocationPermission permission;
     permission = await Geolocator.checkPermission();
     if (permission == LocationPermission.denied) {
       permission = await Geolocator.requestPermission();
     }

    final geoposition = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best);

    setState(() {
      maplat = geoposition.latitude;
      maplong = geoposition.longitude;
    });
  }

  getallregisterlocation() async{
    var temp;
    var params = {"imeino": device_imei_no};
    APICall.returnAPI("getcordinates", context, params).then((apiHitData1) {
      if (this.mounted) {
        setState(() {
          temp = json.decode(apiHitData1);
          if (temp["status_code"] == '200') {
            // setState(() {
            //   print("yashhoooooooo");
            //   print(temp);
            //   RegisterlocationModel register =
            //       RegisterlocationModel.fromJson(temp);
            //
            //   for (var i = 0; i < register.data.locationArray.length; i++) {
            //     _markers.add(
            //       Marker(
            //         markerId: MarkerId('master' + i.toString()),
            //         position: LatLng(
            //             double.parse(
            //                 temp['data']['locationArray'][i]['latitude']),
            //             double.parse(
            //                 temp['data']['locationArray'][i]['longitude'])),
            //                 icon: pinLocationIcon,
            //         visible: true,
            //         infoWindow: InfoWindow(title:  temp['data']['locationArray'][i]['name']),
            //
            //       ),
            //     );print("yash");
            //     if(register.data.locationArray.length-1==i)
            //     register_location_name+=temp['data']['locationArray'][i]['name'];
            //     else
            //       register_location_name+=temp['data']['locationArray'][i]['name']+",";
            //     print(register_location_name);
            //   }
            // });
          } else {
           
          }
        });
      }
    });
  }
  void setCustomMapPin() async {
      pinLocationIcon = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(devicePixelRatio: 5.0,size: Size(5.0,5.0)),
      'assets/images/icons/location_pointer.png');
   }
  _startUp() async {
   

    List<CameraDescription> cameras = await availableCameras();

    /// takes the front camera
    cameraDescription = cameras.firstWhere(
      (CameraDescription camera) =>
          camera.lensDirection == CameraLensDirection.front,
    );

    var appDir = (await getTemporaryDirectory()).path;

    Directory(appDir).delete(recursive: true);
    // start the services
    await _faceNetService.loadModel();
    await _dataBaseService.loadDB();
    _mlVisionService.initialize();

   
  }

  void dispose() {
    super.dispose();
    if (positionStream != null) {
      positionStream.cancel();
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
          appBar: AppBar(
            title: Text("Mark Attendance"),
            automaticallyImplyLeading: false,
            actions: [
              new IconButton(
                icon: new Icon(Icons.power_settings_new),
                onPressed: () async {
                  SharedPreferences prefs =
                      await SharedPreferences.getInstance();
                  prefs.setString("username", "");
                  prefs.setString("password", "");
                  prefs.setString("instid", "");
                  prefs.setString("orgid", "");
                  prefs.setString("loginid", "");
                  Navigator.pushNamed(context, "/Login");
                },
              ),
            ],
            flexibleSpace: Container(
              decoration: BoxDecoration(
                  gradient:
                      LinearGradient(colors: [Colors.blue.shade900, Colors.blue])),
            ),
          ),
          body: 
          loading || maplat==0 || maplong==0?
         Center(
                child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.orange),
              ))
          :
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView(
              physics: NeverScrollableScrollPhysics(),
              padding: EdgeInsets.symmetric(vertical: 20.0),
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 10.0),
                          child: Text(
                            "Hi.. " +
                                loginname.toUpperCase()[0] +
                                loginname
                                    .toUpperCase()
                                    .substring(1)
                                    .toLowerCase() +
                                '😃',
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 20.0,
                            ),
                          ),
                        )
                      ],
                    ),

                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(bottom: 0.0),
                              child: Intime == ''
                                  ? Column(
                                children: [
                                  Row(children: [
                              Container( width: setWidthwithcontex(100.0,context),
                              child: Text(""),
          ),]
                                  ),
                                ],
                              )
                                  :
                              Column(
                                children: [
                                  Row(children: [

                                    Column(children: [  Text("InTime",style: TextStyle(fontSize: 30.0))],)
                                  ],
                                  ), Row(children: [

                                    Column(children: [ Text(Intime.contains(":")? Intime.split(":")[0]+":"+Intime.split(":")[1]:"",style: TextStyle(fontSize: 30.0))],)
                                  ],
                                  ),
                                ],
                              )

                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 0.0),
                              child: profile_photo == ''
                                  ? CircleAvatar(
                                  minRadius: 50,
                                  backgroundImage: AssetImage(
                                      'assets/images/icons/user.png'))
                                  : CircleAvatar(
                                minRadius: 50,
                                backgroundImage:
                                NetworkImage(profile_photo),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 0.0),
                              child: Outtime == ''
                            ?  Column(
                                children: [
                                  Row(children: [
                                    Container( width: setWidthwithcontex(100.0,context),
                                      child: Text(""),
                                    ),]
                                  ),
                                ],
                              )
                                :
                            Column(
                                children: [
                                Row(children: [

                                Column(children: [  Text("OutTime",style: TextStyle(fontSize: 30.0))],)
                  ],
                ), Row(children: [

                                    Column(children: [ Text(Outtime.contains(":")? Outtime.split(":")[0]+":"+Outtime.split(":")[1]:"",style: TextStyle(fontSize: 30.0))],)

                                  ],
                ),
              ],
            )
                            ),
                          ],
                        ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        isregistrationlocation
                            ? !showcontainer
                                ? Container(
                                    height: setHeightwithcontex(70.0, context),
                                    width: MediaQuery.of(context).size.width -
                                        setWidthwithcontex(180, context),
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 30.0),
                                      child: RaisedButton(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(18.0),
                                            side: BorderSide(
                                                color: Colors.white)),
                                        onPressed: () {
                                          Navigator.pushNamed(
                                            context, "/locationregister",
                                            //  "/Registration"
                                          );
                                        },
                                        color: Colors.green[900],
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "Register Location",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                            Icon(
                                              Icons.location_on,
                                              color: Colors.white,
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                : Container()
                            : Container()
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        isregistration
                            ? !showcontainer
                                ? Container(
                                      height: setHeightwithcontex(70.0, context),
                                    width: MediaQuery.of(context).size.width -
                                        setWidthwithcontex(180, context),
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 30.0),
                                      child: RaisedButton(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(18.0),
                                            side: BorderSide(
                                                color: Colors.white)),
                                        onPressed: () {
                                          // Navigator.pushNamed(context, "/register-verifyotp",
                                          //     //  "/Registration"
                                          //     );
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  verifyotp(
                                                cameraDescription:
                                                    cameraDescription,
                                              ),
                                            ),
                                          );
                                        },
                                        color: Colors.red[900],
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "Register Employee",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                            Icon(
                                              Icons.app_registration,
                                              color: Colors.white,
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                : Container()
                            : Container()
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        isattendance
                            ? showcontainer
                                ? Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(20.0),
                                        child: Container(
                                            child: Center(
                                          child: CircularPercentIndicator(
                                            animation: true,
                                            animateFromLastPercent: true,
                                            footer: Text("Capturig Location"),
                                            radius: 100.0,
                                            lineWidth: 10.0,
                                            percent: percentprogress,
                                            center: new Icon(
                                              Icons.location_on,
                                              size: 50.0,
                                              color: Colors.blue[900],
                                            ),
                                            backgroundColor: Colors.grey,
                                            progressColor: Colors.blue[900],
                                          ),
                                        )),
                                      )
                                    ],
                                  )
                                : Container(
                                     height: setHeightwithcontex(70.0, context),
                                    width: MediaQuery.of(context).size.width -
                                        setWidthwithcontex(180, context),
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 30.0),
                                      child: RaisedButton(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(18.0),
                                            side: BorderSide(
                                                color: Colors.white)),
                                        onPressed: () async {
                                          setState(() {
                                            showcontainer = true;
                                          });
                                          percentprogress = 0.0;
                                          var avg_lat = [];
                                          var avg_log = [];

                                          positionStream =
                                              Geolocator.getPositionStream(locationSettings: LocationSettings(accuracy: LocationAccuracy.best),
                                                    )
                                                  .listen((Position position) {
                                            avg_lat.add(position.latitude);
                                            avg_log.add(position.longitude);
                                            var temp =
                                                avg_lat.length.toDouble() / no_of_geoloaction_compare;
                                            if (temp <= 1.0) {
                                              setState(() {
                                                percentprogress = temp;

                                                _markers.add(
                                                  Marker(
                                                    markerId: MarkerId('ME'),
                                                    position: LatLng(
                                                        position.latitude,
                                                        position.longitude),
                                                    visible: true,
                                                  ),
                                                );
                                              });
                                            } else {
                                              positionStream.cancel();
                                              var hello =
                                                  Common.avg(avg_lat, avg_log)
                                                      .split(":");

                                              getcordinates(hello[0], hello[1]);
                                              setState(() {
                                                showcontainer = false;
                                              });
                                            }
                                          });
                                        },
                                        color: Colors.blue[900],
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "Mark Attendance",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                            Icon(
                                              Icons.event_available,
                                              color: Colors.white,
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                            : Container(),
                      ],
                    ),
                    Container(
                      width: setWidthwithcontex(250.0, context),
                        height: setHeightwithcontex(250.0, context),
                      child: Card(elevation: 6.0,
                                              child: GoogleMap(
                          zoomGesturesEnabled: true,
                          myLocationButtonEnabled: true,
                          mapType: MapType.normal,
                          initialCameraPosition: CameraPosition(
                              target: LatLng(maplat, maplong), zoom: 18.56),
                          myLocationEnabled: true,
                          markers: _markers,
                          indoorViewEnabled: true,
                        ),
                      ),
                    ),
                    Container(child: Text("lat= " + avglat)),
                    Container(child: Text("long= " + avglong)),
                    Container(child: Text("Distance= " + distance)),
                  ],
                ),
              ],
            ),
          )),
    );
  }

  _setLoading(bool value) {
    setState(() {
      loading = value;
    });
  }

  void getcordinates(lat, long) {
    print(lat);
    print(long);
    var fetchdata;
    var temp;
    var params = {"imeino": device_imei_no};
    APICall.returnAPI("getcordinates", context, params).then((apiHitData1) {
      if (this.mounted) {
        setState(() {
          temp = json.decode(apiHitData1);
          if (temp["status_code"] == '200') {
            setState(() {
              fetchdata = temp;

              avglat = lat;
              avglong = long;
              var dis = Common.locationcompare(
                  lat, long, fetchdata['data']['locationArray']);

              if (dis) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => photocompare(
                      cameraDescription: cameraDescription,
                    ),
                  ),
                );
              } else {
                toastFunctioncolor(
                    context, "Please try at your registered location. You are away from your registered locations("+register_location_name+")", Colors.red);
              }
            });
          } else {
            toastFunctioncolor(context, temp['message'], Colors.red);
          }
        });
      }
    });
  }
}

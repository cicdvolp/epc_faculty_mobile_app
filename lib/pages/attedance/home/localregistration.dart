import 'dart:async';
import 'dart:convert';


import 'package:epc_faculty/controller/apicall.dart';
import 'package:epc_faculty/pages/attedance/home/avglocation.dart';
import 'package:epc_faculty/pages/attedance/model/locationmastermodel.dart';
import 'package:epc_faculty/pages/attedance/view/Config/global.dart';
import 'package:epc_faculty/pages/css/common_ui.dart';
import 'package:epc_faculty/pages/css/textcss.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:percent_indicator/percent_indicator.dart';

// ignore: camel_case_types
class locationregister extends StatefulWidget {
  locationregister({Key? key}) : super(key: key);

  @override
  _locationregisterState createState() => _locationregisterState();
}

// ignore: camel_case_types
class _locationregisterState extends State<locationregister> {
  late var employability;
  late StreamSubscription<Position> positionStream;
  var percentprogress = 0.0;
  bool fetching = false;
  bool fetchingreg = false;
  bool fetching1 = false;
  var data;
  var fetchdata;
  var regEmployeelist;
  var localtionmasterid;
  String latituteData = "";
  String longitudeData = "";
  bool capture_location = false;
  @override
  bool isinitdone = false;
  void initState() {
    super.initState();
    setState(() {
      isinitdone = true;
    });
    getdata();
    getallmasterinfo();
    setState(() {
      isinitdone = false;
    });
  }

  void dispose() {
    super.dispose();
    if (positionStream != null) {
      positionStream.cancel();

    }
  }

  Future<void> getallmasterinfo() async {
    var temp;
    APICall.returnAPI(
      "getalllocationmasterinfo",
      context,
    ).then((apiHitData1) {
      if (this.mounted) {
        setState(() {
          temp = json.decode(apiHitData1);
          if (temp["status_code"] == '200') {
            setState(() {
              fetchdata = temp;
            });
          } else {
            toastFunctioncolor(context, temp['message'], Colors.red);
          }
        });
      }
    });
  }

  bool getdetails = false;
  Future<void> getdata() async {
    setState(() {
      getdetails = false;
    });
    APICall.returnAPI(
      "getalllocationmaster",
      context,
    ).then((apiHitData) {
      if (this.mounted) {
        setState(() {
          data = json.decode(apiHitData);
          if (data["status_code"] == '200') {
            employability = data;
          } else {
            setState(() {
              getdetails = true;
            });
            // toastFunctioncolor(context, data['message'], Colors.red);
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Location Registration"),
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient:
                    LinearGradient(colors: [Colors.blue.shade50, Colors.blue])),
          ),
        ),
        floatingActionButton: !capture_location
            ? FloatingActionButton(
                child: Icon(Icons.add),
                onPressed: () {
                  setState(() {
                    capture_location = true;
                  });
                  percentprogress = 0.0;
                  var avg_lat = [];
                  var avg_log = [];

                  positionStream = Geolocator.getPositionStream(
                          locationSettings: LocationSettings(accuracy: LocationAccuracy.best))
                      .listen((Position position) {
                    avg_lat.add(position.latitude);
                    avg_log.add(position.longitude);
                    var temp = avg_lat.length.toDouble() / no_of_geoloaction_compare;
                    if (temp <= 1.0) {
                      setState(() {
                        percentprogress = temp;
                      });
                    } else {
                      positionStream.cancel();
                      var lat_long = Common.avg(avg_lat, avg_log).split(":");
                      //  getcordinates(hello[0],hello[1]);

                      addexp("Add location Master", false, null, '',
                          lat_long[0], lat_long[1]);

                      setState(() {
                        capture_location = false;
                      });
                    }
                  });
                },
              )
            : Container(),
        body: isinitdone
            ? Center(
                child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.orange),
              ))
            : capture_location
                ? Container(
                    child: Center(
                    child: CircularPercentIndicator(
                      animation: true,
                      animateFromLastPercent: true,
                      footer: Text("Capturig Location"),
                      radius: 100.0,
                      lineWidth: 10.0,
                      percent: percentprogress,
                      center: new Icon(
                        Icons.location_on,
                        size: 50.0,
                        color: Colors.blue[900],
                      ),
                      backgroundColor: Colors.grey,
                      progressColor: Colors.blue[900],
                    ),
                  ))
                : getdetails
                    ? Center(child: Text(data['message']))
                    : employability == null
                        ? Center(
                            child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.orange),
                          ))
                        : Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 50.0, vertical: 10.0),
                                child: Container(
                                  decoration: ShapeDecoration(
                                    shape: RoundedRectangleBorder(
                                      side: BorderSide(
                                          width: 1.0,
                                          style: BorderStyle.solid,
                                          color: Colors.blue),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(5.0),
                                      ),
                                    ),
                                  ),
                                  child: Container(
                                    margin: EdgeInsets.only(
                                        left: setWidthwithcontex(10, context),
                                        right: setWidthwithcontex(10, context)),
                                    child:  DropdownButton<String>(
                                      underline: SizedBox(),
                                      isExpanded: true,
                                      icon: Icon(
                                        Icons.arrow_drop_down,
                                        color: Colors.yellow,
                                        size: setHeightwithcontex(40, context),
                                      ),
                                      // icon: Icon(Icons.arrow_drop_down_circle, size: setHeight(20),),
                                      hint: new Text("Select Location Master"),
                                      value: localtionmasterid,
                                      onChanged: (newValue) {
                                        setState(() {
                                          localtionmasterid = newValue;
                                          getlocationinformation(
                                              localtionmasterid);
                                        });
                                      },

                                      items: employability
                                          .data.locationMastersList
                                          .map((user) {
                                        return  DropdownMenuItem(
                                          value: user.id.toString(),
                                          child: Row(
                                            children: <Widget>[
                                              Expanded(
                                                child: new Text(
                                                  user.location,
                                                  style: TextStyle(
                                                      fontSize:
                                                          setFontSizewithcontex(
                                                              14, context),
                                                      color: Colors.black),
                                                  overflow: TextOverflow.clip,
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                ),
                              ),
                              fetching1
                                  ? const Center(
                                      child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          Colors.orange),
                                    ))
                                  : Expanded(
                                      child: ListView.builder(
                                        itemCount: fetchdata != null &&
                                                fetchdata['data'] != null
                                            ? fetchdata['data']
                                                    ['location_coordinate_list']
                                                .length
                                            : 0,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return fetchdata != 'null'
                                              ? Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: Card(
                                                    elevation: 6.0,
                                                    child: Container(
                                                      height:
                                                          setHeightwithcontex(
                                                              200.0, context),
                                                      child: Column(
                                                        children: [
                                                          Container(
                                                            decoration: BoxDecoration(
                                                                gradient:
                                                                    LinearGradient(
                                                                        colors: [
                                                                  Colors.blue.shade50,
                                                                  Colors.blue
                                                                ])),
                                                            height:
                                                                setHeightwithcontex(
                                                                    50.0,
                                                                    context),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Padding(
                                                                    padding:
                                                                        const EdgeInsets.all(
                                                                            8.0),
                                                                    child: fetchdata ==
                                                                            null
                                                                        ? Container()
                                                                        : Row(children: [
                                                                            Text(
                                                                              fetchdata['data']['location_coordinate_list'][index]['locationmaster']['location'].toString(),
                                                                              style: TextStyle(color: Colors.white, fontSize: setFontSizewithcontex(20.0, context)),
                                                                            ),
                                                                            FlatButton(
                                                                              onPressed: () async {
                                                                                //  await getemployeelist(
                                                                                //     fetchdata['data']['location_coordinate_list'][index]['locationmaster']['id'],
                                                                                //   );
                                                                                // print(regEmployeelist);

                                                                                getemployeelist(fetchdata['data']['location_coordinate_list'][index]['locationmaster']['id'], fetchdata['data']['location_coordinate_list'][index]['locationmaster']['location']);
                                                                                // displayRegPersonList(fetchdata['data']['location_coordinate_list'][index]['locationmaster']['location'], fetchdata['data']['location_coordinate_list'][index]['locationmaster']['id']);
                                                                              },
                                                                              child: Text(
                                                                                " (" + fetchdata['data']['location_coordinate_list'][index]['registrationcount'].toString() + ")",
                                                                                style: TextStyle(color: Colors.white, fontSize: setFontSizewithcontex(20.0, context)),
                                                                              ),
                                                                            ),
                                                                          ])),
                                                                Row(
                                                                  children: [
                                                                    Padding(
                                                                      padding: const EdgeInsets
                                                                              .only(
                                                                          bottom:
                                                                              5.0),
                                                                      child:
                                                                          IconButton(
                                                                        icon:
                                                                            Icon(
                                                                          Icons
                                                                              .edit,
                                                                          color:
                                                                              Colors.lightGreen,
                                                                        ),
                                                                        onPressed:
                                                                            () {
                                                                          setState(
                                                                              () {
                                                                            capture_location =
                                                                                true;
                                                                          });
                                                                          percentprogress =
                                                                              0.0;
                                                                          var avg_lat =
                                                                              [];
                                                                          var avg_log =
                                                                              [];

                                                                          positionStream =
                                                                              Geolocator.getPositionStream(locationSettings: const LocationSettings(accuracy: LocationAccuracy.best)).listen((Position position) {
                                                                            avg_lat.add(position.latitude);
                                                                            avg_log.add(position.longitude);
                                                                            var temp =
                                                                                avg_lat.length.toDouble() / no_of_geoloaction_compare;
                                                                            if (temp <=
                                                                                1.0) {
                                                                              setState(() {
                                                                                percentprogress = temp;
                                                                              });
                                                                            } else {
                                                                              positionStream.cancel();
                                                                              var lat_long = Common.avg(avg_lat, avg_log).split(":");
                                                                              //  getcordinates(hello[0],hello[1]);

                                                                              addexp("Edit location Master", true, fetchdata['data']['location_coordinate_list'][index]['locationmaster']['id'], fetchdata['data']['location_coordinate_list'][index]['locationmaster']['location'],   lat_long[0], lat_long[1]);

                                                                              setState(() {
                                                                                capture_location = false;
                                                                              });
                                                                            }
                                                                          });
                                                                        },
                                                                      ),
                                                                    ),
                                                                    fetchdata['data']['location_coordinate_list'][index]
                                                                            [
                                                                            'delete']
                                                                        ? IconButton(
                                                                            icon:
                                                                               const Icon(
                                                                              Icons.delete,
                                                                              color: Colors.red,
                                                                            ),
                                                                            onPressed:
                                                                                () {
                                                                              setState(() {
                                                                                fetching1 = true;
                                                                              });
                                                                              var params = {
                                                                                'masterid': fetchdata['data']['location_coordinate_list'][index]['locationmaster']['id'],
                                                                              };
                                                                              APICall.returnAPI("deletemaster", context, params).then((apiHitData) {
                                                                                if (this.mounted) {
                                                                                  setState(() {
                                                                                    data = json.decode(apiHitData);

                                                                                    if (data["status"] == '200') {
                                                                                      fetchdata = data;
                                                                                      localtionmasterid = null;
                                                                                      getdata();
                                                                                      getallmasterinfo();
                                                                                    } else {
                                                                                      // toastFunctioncolor(
                                                                                      //     context,
                                                                                      //     data['msg'],
                                                                                      //     Colors.red);

                                                                                    }
                                                                                    setState(() {
                                                                                      fetching1 = false;
                                                                                    });
                                                                                  });
                                                                                }
                                                                              });
                                                                            },
                                                                          )
                                                                        : Container()
                                                                  ],
                                                                )
                                                              ],
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(8.0),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                Text(
                                                                  "longitude",
                                                                ),
                                                                fetchdata ==
                                                                        null
                                                                    ? Container()
                                                                    : Text(
                                                                        fetchdata['data']['location_coordinate_list'][index]['longitude']
                                                                            .toString(),
                                                                      )
                                                              ],
                                                            ),
                                                          ),
                                                          Divider(
                                                            indent: 30.0,
                                                            endIndent: 30.0,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(8.0),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                Text(
                                                                  "latitude",
                                                                ),
                                                                fetchdata ==
                                                                        null
                                                                    ? Container()
                                                                    : Text(
                                                                        fetchdata['data']['location_coordinate_list'][index]['latitude']
                                                                            .toString(),
                                                                      )
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              : Container();
                                        },
                                      ),
                                    )
                            ],
                          ));
  }

  Future<void> getlocationinformation(localtionmasterid) async {
    setState(() {
      fetching = true;
    });
    var params = {'locationmasterid': localtionmasterid};
    APICall.returnAPI("getlocationinformation", context, params)
        .then((apiHitData) {
      if (this.mounted) {
        setState(() {
          data = json.decode(apiHitData);
          setState(() {
            fetching = false;
          });
          if (data["status_code"] == '200') {
            fetchdata = data;
          } else {
            toastFunctioncolor(context, data['message'], Colors.red);
          }
        });
      }
    });
  }

  addexp(String title, bool edit, var id, String name, var lat, var long) {
    getCurrentLocation();
    var location_name = name;
    var locationid;
    bool saving = false;
    TextEditingController controller = TextEditingController(text: name);
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("Cancel")),
              RaisedButton(
                color: Colors.blue[900],
                onPressed: () {
                  if (edit) {
                    if (latituteData != '' && longitudeData != '') {
                      setState(() {
                        saving = true;
                      });
                      var params = {
                        'location_name': controller.text,
                        "longitude": longitudeData,
                        "latitude": latituteData,
                        "masterid": id,
                      };
                      APICall.returnAPI("addregisterlocation", context, params)
                          .then((apiHitData) {
                        if (this.mounted) {
                          setState(() {
                            data = json.decode(apiHitData);

                            if (data["status"] == '200') {
                              getdata();
                              setState(() {
                                saving = false;
                              });
                              getlocationinformation(id);
                              Navigator.of(context).pop();
                            } else {
                              toastFunctioncolor(
                                  context, data['message'], Colors.red);
                            }
                          });
                        }
                      });
                    } else {
                      toastFunctioncolor(
                          context,
                          "Your Loacation Not Turn On Can You Do It.",
                          Colors.orange);
                    }
                  } else {
                    if (lat != '' && long != '') {
                      setState(() {
                        saving = true;
                      });
                      var params = {
                        'location_name': controller.text,
                        "longitude": long,
                        "latitude": lat
                      };
                      APICall.returnAPI("addregisterlocation", context, params)
                          .then((apiHitData) {
                        if (this.mounted) {
                          setState(() {
                            data = json.decode(apiHitData);

                            if (data["status"] == '200') {
                              getdata();

                              getlocationinformation(data['masterid']);
                              setState(() {
                                // localtionmasterid = data['masterid'].toString();
                              });

                              setState(() {
                                saving = false;
                              });
                              Navigator.of(context).pop();
                              toastFunctioncolor(
                                  context, data['msg'], Colors.green);
                            } else {
                              if (data != null) {
                                Navigator.of(context).pop();
                                toastFunctioncolor(
                                    context, data['msg'], Colors.red);
                              }
                            }
                          });
                        }
                      });
                    } else {
                      toastFunctioncolor(
                          context,
                          "Your Loacation Not Turn On Can You Do It.",
                          Colors.orange);
                    }
                  }
                },
                child: Text(
                  "Save",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
            content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return SingleChildScrollView(
                child: Column(
                  children: [
                    Row(
                      children: [Text(title)],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                            width: setWidthwithcontex(230.0, context),
                            child: TextField(
                              decoration:
                                  InputDecoration(labelText: "Location Name"),
                              // onChanged: (String value) {
                              //   setState(() {
                              //     location_name = value;
                              //   });
                              // },
                              controller: controller,
                            )),
                        // Container(
                        //   width: setWidthwithcontex(230.0, context),
                        //   decoration: ShapeDecoration(
                        //     shape: RoundedRectangleBorder(
                        //       side: BorderSide(
                        //           width: 2.0,
                        //           style: BorderStyle.solid,
                        //           color: Colors.blue),
                        //       borderRadius: BorderRadius.all(
                        //         Radius.circular(5.0),
                        //       ),
                        //     ),
                        //   ),
                        //   child: Container(
                        //     margin: EdgeInsets.only(
                        //         left: setWidthwithcontex(10, context),
                        //         right: setWidthwithcontex(10, context)),
                        //     child: new DropdownButton<String>(
                        //       underline: SizedBox(),
                        //       isExpanded: true,
                        //       icon: Icon(
                        //         Icons.arrow_drop_down,
                        //         color: Colors.yellow[900],
                        //         size: setHeightwithcontex(40, context),
                        //       ),
                        //       // icon: Icon(Icons.arrow_drop_down_circle, size: setHeight(20),),
                        //       hint: new Text(
                        //         "Select Location Master",
                        //         style: TextStyle(
                        //             fontSize:
                        //                 setFontSizewithcontex(12, context)),
                        //       ),
                        //       value: locationid,
                        //       onChanged: (String newValue) {
                        //         setState(() {
                        //           locationid = newValue;
                        //           // getlocationinformation(localtionmasterid);
                        //         });
                        //       },

                        //       items: employability.data.locationMastersList
                        //           .map((user) {
                        //         return new DropdownMenuItem(
                        //           value: user.id.toString(),
                        //           child: Row(
                        //             children: <Widget>[
                        //               Expanded(
                        //                 child: new Text(
                        //                   user.location,
                        //                   style: TextStyle(
                        //                       fontSize: setFontSizewithcontex(
                        //                           14, context),
                        //                       color: Colors.black),
                        //                   overflow: TextOverflow.clip,
                        //                 ),
                        //               ),
                        //             ],
                        //           ),
                        //         );
                        //       }).toList(),
                        //     ),
                        //   ),
                        // ),
                      ],
                    ),
                  ],
                ),
              );
            }));
      },
    );
  }

  displayRegPersonList(String name, var id) {
    // Future.delayed(Duration(seconds: 3), () {
      return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(title: Text(name),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text("Cancel")),
              ],
              content: StatefulBuilder(
                  builder: (BuildContext context, StateSetter setState) {
                return Container(
                  height: setHeightwithcontex(300.0,context), // Change as per your requirement
                  width: setWidthwithcontex(300.0,context), // Change as per your requirement
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: regEmployeelist.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Text(regEmployeelist[index]);
                    },
                  ),
                );
              }));
        },
      );
    // });
    // regEmployeelist=regEmployeelist;
  }

  getCurrentLocation() async {
    final geoposition = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best);

    setState(() {
      latituteData = '${geoposition.latitude}';
      longitudeData = '${geoposition.longitude}';
    });
  }

  getemployeelist(id, name) {
    regEmployeelist = [];
    // setState(() {
    //   fetchingreg = true;
    // });

    var params = {"id": id};
    APICall.returnAPI("getRegEmployeeList", context, params).then((apiHitData) {
      if (this.mounted) {
        // setState(() {
        //   fetchingreg = false;
        // });
        data = json.decode(apiHitData);
        if (data["status_code"] == '200') {
          regEmployeelist = data['data']['employeelist'];
          print(regEmployeelist);
          displayRegPersonList(name, id);
        } else {
          toastFunctioncolor(context, data['message'], Colors.red);
        }
      }
    });
    // var data= '{"data":{"employeelist":["9 : KOMAL UDAY SHINDE"]},"message":"Success","status_code":"200"}'.toString();
    // var data1 = json.decode(data);
    //  regEmployeelist = data1['data']['employeelist'];
  }
}

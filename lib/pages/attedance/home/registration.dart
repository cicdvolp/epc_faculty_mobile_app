import 'dart:convert';
import 'dart:io';

import 'package:epc_faculty/controller/apicall.dart';
import 'package:epc_faculty/pages/attedance/db/database.dart';
import 'package:epc_faculty/pages/attedance/home/photocapture.dart';
import 'package:epc_faculty/pages/attedance/services/facenet.service.dart';
import 'package:epc_faculty/pages/attedance/services/ml_vision_service.dart';
import 'package:epc_faculty/pages/attedance/view/Config/global.dart';
import 'package:epc_faculty/pages/css/Colors.dart';
import 'package:epc_faculty/pages/css/common_ui.dart';
import 'package:epc_faculty/pages/css/textcss.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:multiselect_formfield/multiselect_formfield.dart';
import 'package:geolocator/geolocator.dart';
import 'package:camera/camera.dart';


class Registration extends StatefulWidget {
  final CameraDescription cameraDescription;

  const Registration({Key? key, required this.cameraDescription})
      : super(key: key);
  @override
  _RegistrationState createState() => _RegistrationState();
}

class _RegistrationState extends State<Registration> {
  var email = "";
  var isdetailsavailable = false;
  var shift = "";
  var employeetype = "";
  var fullname = "";
  var geolocation = "";
  var imeino = "";
  var mobile = "";
  var orgaizationname = "";
  var designation = "";
  var department = "";
  var employeecode = "";
  var profilephotourl = "";
  var msg = "";
  bool iserror = false;
 late List localtionmasterid;
 late String _myActivitiesResult;
  final formKey = new GlobalKey<FormState>();

  FaceNetService _faceNetService = FaceNetService();
  MLVisionService _mlVisionService = MLVisionService();
  DataBaseService _dataBaseService = DataBaseService();
  late bool loading;
  late CameraDescription cameraDescription;

  String _macaddr = "UNKNOWN";
  String _imeino = "UNKNOWN";

  String latituteData = "";
  String longitudeData = "";
  bool fetching = false;
  bool fetching1 = false;
  var employability;

  @override
  void initState() {
    super.initState();
    localtionmasterid = [];
    _myActivitiesResult = '';
    clear();
    getdata();
    getCurrentLocation();
    setState(() {
      _macaddr = device_Mac_address;
      _imeino = device_imei_no;
    });
  }

  _saveForm() {
    var form = formKey.currentState;
    if (form==null?false:form.validate()) {
      form.save();
      setState(() {
        _myActivitiesResult = localtionmasterid.toString();
      });
    }
  }

  bool getdetails = false;
  Future<void> getdata() async {
    setState(() {
      getdetails = false;
    });
    APICall.returnAPI(
      "getalllocationmaster",
      context,
    ).then((apiHitData) {
      if (this.mounted) {
        setState(() {
          var data = json.decode(apiHitData);
          if (data["status_code"] == '200') {
            employability = data['data']['location_masters_list'];

            setState(() {
              getdetails = true;
            });
          } else {
            setState(() {
              iserror = true;
              msg = data['message'];
            });
            // toastFunctioncolor(context, data['message'], Colors.red);
          }
        });
      }
    });
  }

  clear() async {
    var appDir = (await getTemporaryDirectory()).path;

    Directory(appDir).delete(recursive: true);
  }

  _startUp() async {
    _setLoading(true);

    List<CameraDescription> cameras = await availableCameras();

    /// takes the front camera
    cameraDescription = cameras.firstWhere(
      (CameraDescription camera) =>
          camera.lensDirection == CameraLensDirection.front,
    );

    // start the services
    await _faceNetService.loadModel();
    await _dataBaseService.loadDB();
    _mlVisionService.initialize();

    _setLoading(false);
  }

  getCurrentLocation() async {
    final geoposition = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best);
    setState(() {
      latituteData = '${geoposition.latitude}';
      longitudeData = '${geoposition.longitude}';
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: SafeArea(
        child: iserror
            ? Scaffold(
                body: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(child: Text(msg)),
                  Container(
                    width: setWidthwithcontex(110, context),
                    child: RaisedButton(
                      color: Colors.blue[900],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.white)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Home',
                            style: TextStyle(color: Colors.white),
                          ),
                          Icon(
                            Icons.home,
                            color: Colors.white,
                          ),
                        ],
                      ),
                      onPressed: () async {
                        // Navigator.pushNamed(context, "/Registration");
                        Navigator.pop(context);
                        Navigator.pop(context);
                      },
                    ),
                  ),
                ],
              ))
            : Scaffold(
                appBar: PreferredSize(
                  preferredSize: Size.fromHeight(65.0),
                  child: AppBar(
                    automaticallyImplyLeading: false,
                    // title: Text("Registration"),
                    flexibleSpace: Container(
                      height: 300.0,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [Colors.blue.shade50, Colors.blue]),
                      ),
                      child: Container(
                        width: screenWidth,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          // crossAxisAlignment: CrossAxisAlignment.center,

                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 5.0),
                                  child: Text(
                                    "Registration",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: setFontSizewithcontex(
                                            17.0, context)),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 9.0),
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(50)),
                                    // gradient: LinearGradient(
                                    //     colors: [Colors.blue[900], Colors.blue])
                                    color: Colors.white.withOpacity(0.3)),
                                width: screenWidth -
                                    setWidthwithcontex(170.0, context),
                                height: setHeightwithcontex(100.0, context),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 8.0),
                                  child: TextField(
                                    maxLines: 1,
                                    onChanged: (value) {
                                      setState(() {
                                        email = value;
                                      });
                                    },
                                    decoration: InputDecoration(
                                        suffixIcon: IconButton(
                                          icon: Icon(
                                            Icons.search,
                                            color: Colors.white,
                                          ),
                                          onPressed: () {
                                            fetchemployee();
                                          },
                                        ),
                                        focusedBorder: InputBorder.none,
                                        enabledBorder: InputBorder.none,
                                        fillColor: Colors.white,
                                        hintText: "Email / Emp Code",
                                        hintStyle: TextStyle(
                                            fontSize: setFontSizewithcontex(
                                                15.0, context),
                                            color:
                                                Colors.white.withOpacity(0.6))),
                                    cursorColor: Colors.white,
                                    onSubmitted: (value) {
                                      fetchemployee();
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              child: IconButton(
                                icon: Icon(
                                  Icons.home,
                                  color: Colors.white,
                                ),
                                onPressed: () {
                                  Navigator.pushNamed(context, "/Home");
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    // actions: [
                    //   new IconButton(
                    //     icon: new Icon(Icons.home),
                    //     onPressed: () {
                    //       Navigator.pushNamed(context, "/Home");
                    //     },
                    //   ),
                    // ],
                  ),
                ),
                body: ListView(
                  children: [
                    Container(
                        // child: Column(
                        //   children: [
                        //     Row(
                        //       mainAxisAlignment: MainAxisAlignment.center,
                        //       children: [
                        //         Container(
                        //             width: screenWidth - 100,
                        //             child: Container(
                        //               child: TextField(
                        //                 onChanged: (value) {
                        //                   setState(() {
                        //                     email = value;
                        //                   });
                        //                 },
                        //                 decoration:
                        //                     InputDecoration(labelText: "Email Id"),
                        //               ),
                        //             )),
                        //       ],
                        //     ),
                        //     Container(
                        //       height: 70.0,
                        //       width: MediaQuery.of(context).size.width - 200,
                        //       child: Padding(
                        //         padding: const EdgeInsets.only(top: 30.0),
                        //         child: fetching
                        //             ? Center(
                        //                 child: CircularProgressIndicator(
                        //                 valueColor: AlwaysStoppedAnimation<Color>(
                        //                     Colors.orange),
                        //               ))
                        //             : RaisedButton(
                        //                 shape: RoundedRectangleBorder(
                        //                     borderRadius: BorderRadius.circular(18.0),
                        //                     side: BorderSide(color: Colors.white)),
                        //                 onPressed: () async {
                        //                   fetchemployee();

                        //                   // Navigator.pushNamed(
                        //                   //     context,
                        //                   //     //  "/register-verifyotp"
                        //                   //     "/Registration");
                        //                 },
                        //                 color: Colors.blue,
                        //                 child: Text(
                        //                   "FETCH",
                        //                   style: TextStyle(color: Colors.white),
                        //                 ),
                        //               ),
                        //       ),
                        //     ),
                        //     SizedBox(
                        //       height: 20,
                        //     ),
                        //   ],
                        // ),
                        ),
                    fetching
                        ? Center(
                            child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.orange),
                          ))
                        : isdetailsavailable
                            ? Container(
                                child: Column(
                                  children: [
                                    profilephoto(),
                                    employeeinfo(),
                                  ],
                                ),
                              )
                            : Container()
                  ],
                ),
              ),
      ),
    );
  }

 

  Future<void> fetchemployee() async {
    var data;

    if (_macaddr != "UNKNOWN" && _imeino != "UNKNOWN") {
      // SharedPreferences prefs = await SharedPreferences.getInstance();
      // username = prefs.getString('username');

      if (latituteData == '' || longitudeData == '') {
        toastFunctioncolor(context, "Your Loacation Not Turn On Can You Do It.",
            Colors.orange);
      } else {
        setState(() {
          fetching = true;
        });
        var params2 = {"username": email};
        APICall.returnAPI("fetchemployee", context, params2).then((apiHitData) {
          if (this.mounted) {
            setState(() {
              fetching = false;
              data = json.decode(apiHitData);

              if (data["status_code"] == '200') {
                shift = data['data']['shift'];
                employeetype = data['data']['employeetype'];
                fullname = data['data']['full_name'];
                geolocation = data['data']['biometric_details']['geo_location'];
                imeino = data['data']['biometric_details']['imei_no'];
                mobile = data['data']['mobile'];
                orgaizationname = data['data']['orgaization_name'];
                designation = data['data']['designation'];
                department = data['data']['department'];
                employeecode = data['data']['employee_code'];
                registrationuid = data['data']['email'];
                profilephotourl = data['data']['profile_photo'];
                localtionmasterid = data['data']['biometricRegistrationlist'];

                isdetailsavailable = true;
              } else {
                setState(() {
                  fetching = false;
                });
                toastFunctioncolor(context, data['message'], Colors.red);
              }
            });
          }
        });
      }
    }
  }

  Widget profilephoto() {
    return Center(
      child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.circle,
            boxShadow: [
              BoxShadow(blurRadius: 5.0, color: Colors.grey, spreadRadius: 2.0)
            ],
          ),
          width: screenWidth / 2,
          padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
          child: profilephotourl == '-'
              ? CircleAvatar(
                  minRadius: 100,
                  backgroundImage: AssetImage('assets/images/icons/user.png'))
              : CircleAvatar(
                  minRadius: 100.0,
                  backgroundImage: NetworkImage(profilephotourl),
                )),
    );
  }

  Widget employeeinfo() {
    return Column(
      children: [
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          elevation: 10.0,
          margin: EdgeInsets.all(10.0),
          color: whiteColor,
          child: Container(
            decoration: BoxDecoration(
                gradient:
                    LinearGradient(colors: [Colors.blue.shade50, Colors.blue]),
                borderRadius: BorderRadius.all(Radius.circular(15))),
            width: screenWidth,
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      employeecode + " : " + fullname,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
                Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      department,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
                Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      designation,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
                Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: setWidthwithcontex(300, context),
                      child: Text(
                        orgaizationname,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ],
                ),
                Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      mobile + " : " + registrationuid,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 50.0, vertical: 10.0),
          child: Container(
            decoration: ShapeDecoration(
              shape: RoundedRectangleBorder(
                side: BorderSide(
                    width: 1.0, style: BorderStyle.solid, color: Colors.blue),
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0),
                ),
              ),
            ),
            child: Container(
              margin: EdgeInsets.only(
                  left: setWidthwithcontex(10, context),
                  right: setWidthwithcontex(10, context)),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(16),
                      child: MultiSelectFormField(
                        autovalidate: AutovalidateMode.always,
                        chipBackGroundColor: Colors.blue,
                        chipLabelStyle: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white),
                        dialogTextStyle: TextStyle(fontWeight: FontWeight.bold),
                        checkBoxActiveColor: Colors.blue,
                        checkBoxCheckColor: Colors.white,
                        dialogShapeBorder: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(12.0))),
                        title: Text(
                          "Location Master",
                          style: TextStyle(fontSize: 16),
                        ),
                        validator: (value) {
                          if (value == null || value.length == 0) {
                            return 'Please select one or more location';
                          }
                          return null;
                        },
                        dataSource: employability,
                        textField: 'location',
                        valueField: 'id',
                        okButtonLabel: 'OK',
                        cancelButtonLabel: 'CANCEL',
                        hintWidget: Text('Please choose location'),
                        initialValue: localtionmasterid,
                        onSaved: (value) {
                          if (value == null) return;
                          setState(() {
                            localtionmasterid = value;
                          });
                        },
                      ),
                    ),
                  ]),
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            fetching1
                ? Center(
                    child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.orange),
                  ))
                : Container(
                    height: 70.0,
                    width: MediaQuery.of(context).size.width -
                        setWidthwithcontex(180, context),
                    child: Padding(
                      padding: const EdgeInsets.only(top: 30.0),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.white)),
                        onPressed: () {
                          print(localtionmasterid);
                          if (localtionmasterid .isNotEmpty) {
                            Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => potocapture(
                      cameraDescription: widget.cameraDescription,
                      masterid: localtionmasterid,
                      // msg: data['message'],
                    ),
                  ),
                );
                            // savedeviceinfo();
                          } else {
                            toastFunctioncolor(context,
                                "Please select location Master", Colors.red);
                          }
                        },
                        color: Colors.blue[900],
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Text(
                              "Face Registration.",
                              style: TextStyle(color: Colors.white),
                            ),
                            Icon(
                              Icons.face,
                              color: Colors.white,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
          ],
        )
      ],
    );
  }

  _setLoading(bool value) {
    setState(() {
      loading = value;
    });
  }
}

import 'dart:convert';
import 'dart:math';

import 'package:camera/camera.dart';

import 'package:epc_faculty/pages/attedance/home/registration.dart';
import 'package:epc_faculty/pages/css/common_ui.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class verifyotp extends StatefulWidget {
  // verifyotp({Key key}) : super(key: key);
 final CameraDescription cameraDescription;

  const verifyotp({Key? key, required this.cameraDescription}) : super(key: key);
  @override
  _verifyotpState createState() => _verifyotpState();
}

class _verifyotpState extends State<verifyotp> {
  var mobileotp;
  var emailotp;
  bool sending=false;
  @override
  void initState() {
    super.initState();
setState(() {
  // sending=true;
});
var randomizer = new Random(); 
var eotp='';
var motp='';
  for(var i=0;i<=5;i++)
  {
    eotp=eotp+randomizer.nextInt(9).toString();
    motp=motp+randomizer.nextInt(9).toString();
  }

  eotp='1';
  motp='1';
  storeopt(eotp,motp);

    // var data;
    //  var params = {"mobileotp": motp, "emailotp": eotp};
    // APICall.returnAPI("send-otp", context,params).then((apiHitData) {
    //   if (this.mounted) {
    //     setState(() {
    //       data = json.decode(apiHitData);
    //       if (data["status"] == '200') {
    //         setState(() {
    //                       sending=false;
    //                     });
    //         toastFunctioncolor(context, data['msg'], Colors.green);
    //       } else {
    //         toastFunctioncolor(context, data['msg'], Colors.red);
    //       }
    //     });
    //   }
    // });
    
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
          appBar: AppBar(
            title: Text("OTP Verification"),
            flexibleSpace: Container(
              decoration: BoxDecoration(
                  gradient:
                      LinearGradient(colors: [Colors.blue.shade50, Colors.blue])),
            ),
          ),
          body: 
          sending?
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(child: Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.green),
                  ),
                ),),
                 Container(child: Center(
                  child: Text("Sending Otp..")
                ),),
            ],
          )
          :
          SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          width: MediaQuery.of(context).size.width - 100,
                          height: 300.0,
                          child: Image.asset(
                              'assets/images/icons/otpverification.png')),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width - 100,
                        child: Text(
                            "please enter otp that are send  your mobile no and email. "),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          width: MediaQuery.of(context).size.width - 100,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            onChanged: (value) {
                              setState(() {
                                emailotp = value;
                              });
                            },
                            decoration: InputDecoration(labelText: "EmailOTP"),
                          )),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          width: MediaQuery.of(context).size.width - 100,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            onChanged: (value) {
                              setState(() {
                                mobileotp = value;
                              });
                            },
                            decoration: InputDecoration(labelText: "MobileOTP"),
                          )),
                    ],
                  ),
                  Container(
                    height: 70.0,
                    width: MediaQuery.of(context).size.width - 200,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 30.0),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.white)),
                        onPressed: () async{
                            SharedPreferences prefs = await SharedPreferences.getInstance();
                        
                            if(prefs.getString('eotp')==emailotp.toString() && prefs.getString('motp')==mobileotp.toString())
                            {
                              prefs.remove("eotp");
                              prefs.remove("motp");
                              //    Navigator.pushNamed(
                              //   context,
                              // //  "/register-verifyotp"
                              //   "/Registration"
                              // );
                                 Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext context) => Registration(
                                cameraDescription: widget.cameraDescription,
                              ),
                            ),
                          );
                            }
                            else{
                              toastFunctioncolor(context, "Please enter vaild otp", Colors.red);
                            }
                        },
                        color: Colors.green[900],
                        child: Text(
                          "Verify",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }

  Future<void> storeopt(String eotp,String mopt) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
     prefs.setString("eotp",eotp);
    prefs.setString("motp",mopt);
  }
}

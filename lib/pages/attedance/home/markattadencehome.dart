import 'package:camera/camera.dart';
import 'package:epc_faculty/pages/attedance/db/database.dart';
import 'package:epc_faculty/pages/attedance/home/photocompare.dart';
import 'package:epc_faculty/pages/attedance/services/facenet.service.dart';
import 'package:epc_faculty/pages/attedance/services/ml_vision_service.dart';

import 'package:flutter/material.dart';

// ignore: camel_case_types
class markattendancehome extends StatefulWidget {
  markattendancehome({Key? key}) : super(key: key);

  @override
  _markattendancehomeState createState() => _markattendancehomeState();
}

// ignore: camel_case_types
class _markattendancehomeState extends State<markattendancehome> {

 FaceNetService _faceNetService = FaceNetService();
  MLVisionService _mlVisionService = MLVisionService();
  DataBaseService _dataBaseService = DataBaseService();
late bool loading;
late CameraDescription cameraDescription;
  
@override
void initState() { 
  super.initState();
   _startUp();
}
  _startUp() async {
    _setLoading(true);

    List<CameraDescription> cameras = await availableCameras();

    /// takes the front camera
    cameraDescription = cameras.firstWhere(
      (CameraDescription camera) => camera.lensDirection == CameraLensDirection.front,
    );

    // start the services
    await _faceNetService.loadModel();
    await _dataBaseService.loadDB();
    _mlVisionService.initialize();

    _setLoading(false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     appBar:  AppBar(
        title: Text("Mark Attendance"),
        flexibleSpace: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [Colors.blue.shade50, Colors.blue])),
          ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 70.0,
                    width: MediaQuery.of(context).size.width - 200,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 30.0),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.white)),
                        onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext context) => photocompare(
                                cameraDescription: cameraDescription,
                              ),
                            ),
                          );
                        },
                        color: Colors.blue[900],
                        child: Text(
                          "Face detection",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ],
              )
      ],),
    );
  }
   _setLoading(bool value) {
    setState(() {
      loading = value;
    });
  }
}

import 'dart:async';
import 'dart:convert';




import 'package:epc_faculty/controller/apicall.dart';
import 'package:epc_faculty/pages/attedance/view/Config/global.dart';


import 'package:epc_faculty/pages/css/common_ui.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';

// ignore: camel_case_types
class loginscreen extends StatefulWidget {
  loginscreen({Key? key}) : super(key: key);

  @override
  _loginscreenState createState() => _loginscreenState();
}

// ignore: camel_case_types
class _loginscreenState extends State<loginscreen> {
  var username = "";
  var password = "";
  bool logining = false;
  @override
  void initState() {
    super.initState();

   
    new Timer(new Duration(milliseconds: 40), () {
      var sWidth = MediaQuery.of(context).size.width;
      var sHeight = MediaQuery.of(context).size.height;
      textScaleFactors = MediaQuery.of(context).textScaleFactor;
      screenHeight = sHeight;
      screenWidth = sWidth;
    });
     login();
    initPlatformState();
    
  }

   Future<void> initPlatformState() async {
    String macaddr;
    String imeino;
    try {
      macaddr = "yashmac";
      // imeino = await ImeiPlugin.getImei(shouldShowRequestPermissionRationale: false);
      imeino = "yashimei";
    } on PlatformException {
      macaddr = "Failed to get mac address";
    }
    if (!mounted) return;
    setState(() {
      device_Mac_address = macaddr;
      device_imei_no = "yashimei";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: logining
          ? Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          width: MediaQuery.of(context).size.width - 100,
                          height: 100.0,
                          child: Image.asset(
                              'assets/images/logo/edupluscamp.png')),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Colors.yellow),
                      )
                    ],
                  ),
                ],
              ),
            )
          : Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          width: MediaQuery.of(context).size.width - 100,
                          height: 100.0,
                          child: Image.asset(
                              'assets/images/logo/edupluscamp.png')),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          width: MediaQuery.of(context).size.width - 100,
                          child: TextField(
                            onChanged: (value) {
                              setState(() {
                                username = value;
                              });
                            },
                            decoration: InputDecoration(labelText: "Username"),
                          )),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                          width: MediaQuery.of(context).size.width - 100,
                          child: TextField(
                            onChanged: (value) {
                              setState(() {
                                password = value;
                              });
                            },
                            obscureText: true,
                            decoration: InputDecoration(labelText: "Password"),
                          )),
                    ],
                  ),
                  Container(
                    height: 70.0,
                    width: MediaQuery.of(context).size.width - 200,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 30.0),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.white)),
                        onPressed: () async {
                          if (username != '' && password != '') {
                            SharedPreferences prefs =
                                await SharedPreferences.getInstance();
                            prefs.setString("username", username);
                            prefs.setString("password", password);
                            login();
                          } else {
                            toastFunctioncolor(
                                context,
                                "Please Enter Username And Password",
                                Colors.red);
                          }
                        },
                        color: Colors.blue[900],
                        child: Text(
                          "Login",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
    );
  }

  Future<void> login() async {
    setState(() {
      loginname = '';
      profile_photo = '';
    });
    
    SharedPreferences prefs = await SharedPreferences.getInstance();
    username = prefs.getString('username').toString();
    password = prefs.getString("password").toString();
  
    if (username != null && password != null) {
      var data;
      var params = {"username": username, "password": password};
      setState(() {
        logining = true;
      });
      APICall.returnAPI("login", context, params).then((apiHitData) {
        if (this.mounted) {
          setState(() {
            data = json.decode(apiHitData);
            setState(() {
              logining = false;
            });
            if (data["status"] == '200') {
              setState(() {
                isregistration = data['isregistration'];
                isattendance = data['isattadance'];
                isregistrationlocation = data['isregistrationlocation'];
                //isregistrationlocation=true;
                loginname = data['msg'].toString();
                profile_photo = data['profile_photo'].toString();
                Intime=data['intime']==null?"":data['intime'];
                Outtime=data['outtime']==null?"":data['outtime'];
                compare_distance=data['compare_distance']==null?20:data['compare_distance'];
                no_of_geoloaction_compare=data['no_of_geolocation_fetch']==null?3.0:data['no_of_geolocation_fetch'];
              });
              prefs.setString("loginid", data['loginid'].toString());
              prefs.setString("orgid", data['orgid'].toString());
              prefs.setString("instid", data['instid'].toString());
              Navigator.pushNamed(context, "/Home");
            } else {
              toastFunctioncolor(context, data['msg'], Colors.red);
            }
          });
        }
      });
    }
  }
}

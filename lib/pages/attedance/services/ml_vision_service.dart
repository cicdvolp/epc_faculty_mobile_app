
import 'package:camera/camera.dart';
import 'package:epc_faculty/pages/attedance/services/camera.service.dart';
import 'package:google_ml_vision/google_ml_vision.dart';
import 'package:flutter/material.dart';

class MLVisionService {
 
  static final MLVisionService _cameraServiceService = MLVisionService._internal();

  factory MLVisionService() {
    return _cameraServiceService;
  }

  MLVisionService._internal();

  CameraService _cameraService = CameraService();

  late FaceDetector _faceDetector;
  FaceDetector get faceDetector => this._faceDetector;

  void initialize() {
    this._faceDetector = GoogleVision.instance.faceDetector(
      FaceDetectorOptions(
        mode: FaceDetectorMode.accurate,
      ),
    );
  }

  Future<List<Face>> getFacesFromImage(CameraImage image) async {

GoogleVisionImageMetadata _firebaseImageMetadata = GoogleVisionImageMetadata(
      rotation: _cameraService.cameraRotation,
      rawFormat: image.format.raw,
      size: Size(image.width.toDouble(), image.height.toDouble()),
      planeData: image.planes.map(
        (Plane plane) {
          return GoogleVisionImagePlaneMetadata(
            bytesPerRow: plane.bytesPerRow,
            height: plane.height,
            width: plane.width,
          );
        },
      ).toList(),
    );

    GoogleVisionImage _firebaseVisionImage =
    GoogleVisionImage.fromBytes(image.planes[0].bytes, _firebaseImageMetadata);

    List<Face> faces = await this._faceDetector.processImage(_firebaseVisionImage);
    return faces;


  }
}

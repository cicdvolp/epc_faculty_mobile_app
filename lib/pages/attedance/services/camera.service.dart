import 'dart:ui';

import 'package:camera/camera.dart';
import 'package:google_ml_vision/google_ml_vision.dart';

class CameraService {
  // singleton boilerplate
  static final CameraService _cameraServiceService = CameraService._internal();

  factory CameraService() {
    return _cameraServiceService;
  }

  // singleton boilerplate
  CameraService._internal();
  //
 late CameraController _cameraController;
  CameraController get cameraController => this._cameraController;
  //
 late CameraDescription _cameraDescription;
  //
 late ImageRotation _cameraRotation;
  ImageRotation get cameraRotation => this._cameraRotation;

  Future startService(CameraDescription cameraDescription) async {
    this._cameraDescription = cameraDescription;
    this._cameraController = CameraController(
      this._cameraDescription,
      ResolutionPreset.high,
      enableAudio: false,
    );

    // sets the rotation of the image
    this._cameraRotation = rotationIntToImageRotation(
      this._cameraDescription.sensorOrientation,
    );

    // Next, initialize the controller. This returns a Future.
    return this._cameraController.initialize();
  }

  ImageRotation rotationIntToImageRotation(int rotation) {
    switch (rotation) {
      case 90:
        return ImageRotation.rotation90;
      case 180:
        return ImageRotation.rotation180;
      case 270:
        return ImageRotation.rotation270;
      default:
        return ImageRotation.rotation0;
    }
  }

  /// takes the picture and saves it in the given path 📸
  Future<String> takePicture() async {
   XFile file = await _cameraController.takePicture();
   return file.path;
  }

  /// returns the image size 📏
  Size getImageSize() {
    return Size(
      _cameraController.value.previewSize?.height as double,
      _cameraController.value.previewSize?.width as double,
    );
  }

  dispose() {
    this._cameraController.dispose();
  }
}

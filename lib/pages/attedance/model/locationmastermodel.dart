// class LocationmasterModel {
//   Data? data;
//   String? message;
//   String? statusCode;
//
//   LocationmasterModel({this.data, this.message, this.statusCode});
//
//   LocationmasterModel.fromJson(Map<String, dynamic> json) {
//     data = json['data'] != null ? new Data.fromJson(json['data']) : null;
//     message = json['message'];
//     statusCode = json['status_code'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     if (this.data != null) {
//       data['data'] = this.data.toJson();
//     }
//     data['message'] = this.message;
//     data['status_code'] = this.statusCode;
//     return data;
//   }
// }
//
// class Data {
//   List<LocationMastersList>? locationMastersList;
//
//   Data({this.locationMastersList});
//
//   Data.fromJson(Map<String, dynamic> json) {
//     if (json['location_masters_list'] != null) {
//       locationMastersList = new List<LocationMastersList>();
//       json['location_masters_list'].forEach((v) {
//         locationMastersList.add(new LocationMastersList.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     if (this.locationMastersList != null) {
//       data['location_masters_list'] =
//           this.locationMastersList.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class LocationMastersList {
//   String updationIpAddress;
//   String creationUsername;
//   String updationDate;
//   Organization organization;
//   String location;
//   int id;
//   String creationDate;
//   String updationUsername;
//   String creationIpAddress;
//
//   LocationMastersList(
//       {this.updationIpAddress,
//       this.creationUsername,
//       this.updationDate,
//       this.organization,
//       this.location,
//       this.id,
//       this.creationDate,
//       this.updationUsername,
//       this.creationIpAddress});
//
//   LocationMastersList.fromJson(Map<String, dynamic> json) {
//     updationIpAddress = json['updation_ip_address'];
//     creationUsername = json['creation_username'];
//     updationDate = json['updation_date'];
//     organization = json['organization'] != null
//         ? new Organization.fromJson(json['organization'])
//         : null;
//     location = json['location'];
//     id = json['id'];
//     creationDate = json['creation_date'];
//     updationUsername = json['updation_username'];
//     creationIpAddress = json['creation_ip_address'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['updation_ip_address'] = this.updationIpAddress;
//     data['creation_username'] = this.creationUsername;
//     data['updation_date'] = this.updationDate;
//     if (this.organization != null) {
//       data['organization'] = this.organization.toJson();
//     }
//     data['location'] = this.location;
//     data['id'] = this.id;
//     data['creation_date'] = this.creationDate;
//     data['updation_username'] = this.updationUsername;
//     data['creation_ip_address'] = this.creationIpAddress;
//     return data;
//   }
// }
//
// class Organization {
//   String organizationName;
//   int id;
//
//   Organization({this.organizationName, this.id});
//
//   Organization.fromJson(Map<String, dynamic> json) {
//     organizationName = json['organization_name'];
//     id = json['id'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['organization_name'] = this.organizationName;
//     data['id'] = this.id;
//     return data;
//   }
// }
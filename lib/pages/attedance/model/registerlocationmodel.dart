// class RegisterlocationModel {
//   // Data data;
//   String message;
//   String statusCode;
//
//   RegisterlocationModel({required this.message,required this.statusCode});
//
//   RegisterlocationModel.fromJson(Map<String, dynamic> json) {
//     data = json['data'] != null ? new Data.fromJson(json['data']) : null;
//     message = json['message'];
//     statusCode = json['status_code'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     if (this.data != null) {
//       data['data'] = this.data.toJson();
//     }
//     data['message'] = this.message;
//     data['status_code'] = this.statusCode;
//     return data;
//   }
// }
// //
// // class Data {
// //   List<LocationArray> locationArray;
// //   bool compairGeolocation;
// //
// //   Data({this.locationArray, this.compairGeolocation});
// //
// //   Data.fromJson(Map<String, dynamic> json) {
// //     if (json['locationArray'] != null) {
// //       locationArray = new List<LocationArray>();
// //       json['locationArray'].forEach((v) {
// //         locationArray.add(new LocationArray.fromJson(v));
// //       });
// //     }
// //     compairGeolocation = json['compairGeolocation'];
// //   }
// //
// //   Map<String, dynamic> toJson() {
// //     final Map<String, dynamic> data = new Map<String, dynamic>();
// //     if (this.locationArray != null) {
// //       data['locationArray'] =
// //           this.locationArray.map((v) => v.toJson()).toList();
// //     }
// //     data['compairGeolocation'] = this.compairGeolocation;
// //     return data;
// //   }
// // }
// //
// // class LocationArray {
// //   String latitude;
// //   String longitude;
// //
// //   LocationArray({this.latitude, this.longitude});
// //
// //   LocationArray.fromJson(Map<String, dynamic> json) {
// //     latitude = json['latitude'];
// //     longitude = json['longitude'];
// //   }
// //
// //   Map<String, dynamic> toJson() {
// //     final Map<String, dynamic> data = new Map<String, dynamic>();
// //     data['latitude'] = this.latitude;
// //     data['longitude'] = this.longitude;
// //     return data;
// //   }
// // }
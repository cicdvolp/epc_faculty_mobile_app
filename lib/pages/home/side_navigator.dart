import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SideNavigator extends StatefulWidget {
  SideNavigator({Key? key}) : super(key: key);

  @override
  _SideNavigatorState createState() => _SideNavigatorState();
}

class _SideNavigatorState extends State<SideNavigator> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Side Navigator'),
      ),
    );
  }
}

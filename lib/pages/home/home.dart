import 'dart:convert';

import 'package:epc_faculty/controller/apicall.dart';
import 'package:epc_faculty/pages/css/Colors.dart';
import 'package:epc_faculty/pages/css/textcss.dart';
import 'package:epc_faculty/pages/login/footer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

var isCollapsed = true;
double screensHeight = 0, screensWidth = 0;
var name = "",
    photo = "",
    dept = "",
    loginid = "",
    employee_code = "",
    orgid = "";

var testicons = [
  Icons.person,
  Icons.list,
  Icons.calendar_view_month,
  Icons.input,
  Icons.group,
  Icons.event_note,
  Icons.notifications_outlined,
  Icons.local_library,
  Icons.receipt,
  Icons.restaurant,
  Icons.calendar_today,
  Icons.forum,
  Icons.construction,
  Icons.document_scanner,
  Icons.person_add_outlined,
  Icons.book
];
var testnames = [
  "Attendance",
  "Timesheet",
  "Timetable",
  "Leave",
  "Student Attendance",
  "Lesson Plan",
  "Notice",
  "Library",
  "Salary",
  "Meal menu",
  "Calender",
  "Discussion",
  "Maintainance",
  "Survey",
  "Documents",
  "Visitor",
  "Service Book"
];

var testroutes = [
  "/Attendance",
  "/Timesheet",
  "/Timetable",
  "/Leave",
  "/StudentAttendance",
  "/LessonPlan",
  "/Notice",
  "/Library",
  "/Salary",
  "/Mealmenu",
  "/Calender",
  "/Discussion",
  "/Maintainance",
  "/Survey",
  "/Documents",
  "/Visitor",
  "/ServiceBook"
];

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  var loading = false;
  var gridObjectList = [];
  late AnimationController anim_controller;
  late Animation<double> scaleAnimation;
  @override
  void initState() {
    setState(() {
      isCollapsed = true;
      loading = true;
      anim_controller = AnimationController(
          vsync: this,
          duration: Duration(
            milliseconds: 300,
          ));
      scaleAnimation =
          Tween<double>(begin: 1, end: 0.6).animate(anim_controller);
    });
    getHomeMenu();
    getDetails();
    super.initState();
  }

  @override
  void dispose() {
    anim_controller.dispose();
    super.dispose();
  }

  void getHomeMenu() async {
    Map<String, dynamic> jsonmap = {};
    APICall.returnAPIWithHeader("getHomeIcons", context, jsonmap)
        .then((apiHitData) {
      if (mounted) {
        setState(() {
          final body = jsonDecode(apiHitData);
          if (body['msg'] == "Success") {
            print(body);
            gridObjectList = body['Mobile_app_link_list'];
            print("gridObjectList");
            print(gridObjectList);
            loading = false;
          }
        });
      }
    });
  }

  void getDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    name = prefs.getString("flname").toString();
    photo = prefs.getString("profile_photo").toString();
    loginid = prefs.getString("loginid").toString();
    employee_code = prefs.getString("emp_code").toString();
    orgid = prefs.getString("orgid").toString();
    dept = prefs.getString("dept").toString();
    //prefs.getString("dept").toString();
    prefs.setBool('loggedin', true);
    setState(() {});
  }

  void dropDetails() async {
    print("dropDetails_function");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
    prefs.setBool('loggedin', false);
    Navigator.pushNamed(context, "/Login");
  }

  @override
  Widget build(BuildContext context) {
    screensHeight = MediaQuery.of(context).size.height;
    screensWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: themeColorBlue,
      // backgroundColor: Colors.grey[500],
      body: Stack(
        children: [
          menu(context),
          dashboard(context),
        ],
      ),
    );
  }

  Widget menu(context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, '/ChangePass');
                },
                child: menuItem("Id Card", Icons.badge)),
            const SizedBox(
              height: 10,
            ),
            GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, '/ChangePass');
                },
                child: menuItem("Profile", Icons.person)),
            const SizedBox(
              height: 10,
            ),
            GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, '/ChangePass');
                },
                child: menuItem("Change Password", Icons.vpn_key)),
            const SizedBox(
              height: 10,
            ),
            GestureDetector(child: menuItem("About", Icons.info_outline)),
            const SizedBox(
              height: 10,
            ),
            GestureDetector(child: menuItem("User", Icons.person)),
            const SizedBox(
              height: 10,
            ),
            GestureDetector(child: menuItem("Help Desk", Icons.headphones)),
            const SizedBox(
              height: 10,
            ),
            GestureDetector(
                onTap: () {
                  dropDetails();
                },
                child: menuItem("Logout", Icons.logout_sharp)),
          ],
        ),
      ),
    );
  }

  menuItem(String name, IconData icon) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Icon(
          icon,
          color: Colors.white,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: Text(
            name,
            style: txtStyle(16, Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
      ],
    );
  }

  Widget dashboard(context) {
    return gridObjectList == null
        ? Center(
            child: CircularProgressIndicator(),
          )
        : AnimatedPositioned(
            duration: const Duration(milliseconds: 300),
            top: 0,
            bottom: 0,
            left: isCollapsed ? 0 : 0.6 * screensWidth,
            right: isCollapsed ? 0 : -0.4 * screensWidth,
            child: ScaleTransition(
              scale: scaleAnimation,
              child: Material(
                elevation: 8,
                borderRadius: const BorderRadius.all(Radius.circular(40)),
                child: Container(
                  color: Colors.grey[200],
                  // appBar: AppBar(
                  //   toolbarHeight: 80,
                  //   shape: cardBorder(),
                  //   elevation: 4,
                  //   backgroundColor: themeColorBlue,
                  //   leading: InkWell(
                  //     child: Icon(isCollapsed ? Icons.menu : Icons.arrow_back),
                  //     onTap: () {
                  //       setState(() {
                  //         if (isCollapsed)
                  //           anim_controller.forward();
                  //         else
                  //           anim_controller.reverse();
                  //         isCollapsed = !isCollapsed;
                  //       });
                  //     },
                  //   ),
                  //   title: const Text("Vishwakarma Institute of Technology"),
                  // ),
                  child: Column(
                    children: [
                      Card(
                        color: themeColorBlue,
                        shape: cardBorder(),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 4, right: 4, top: 48, bottom: 25),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  setState(() {
                                    if (isCollapsed)
                                      anim_controller.forward();
                                    else
                                      anim_controller.reverse();
                                    isCollapsed = !isCollapsed;
                                  });
                                },
                                child: isCollapsed
                                    ? Icon(
                                        Icons.menu,
                                        color: Colors.white,
                                      )
                                    : Icon(Icons.arrow_back,
                                        size: 38, color: Colors.white),
                              ),
                              SizedBox(
                                width: isCollapsed ? 0 : 10,
                              ),
                              Container(
                                width: screensWidth * 0.65,
                                child: Text(
                                  "Vishwakarma Institute of Technology",
                                  style: txtStyle(13, Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              const Image(
                                width: 40,
                                image: AssetImage(
                                  'assets/images/logo/edupluscampuswhite.png',
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: ListView(
                          children: [
                            // SizedBox(
                            //   height: setHeight(10),
                            // ),
                            Center(
                              child: Container(
                                height: 160,
                                child: Card(
                                  color: themeColorBlue, //Color(0xFF33FFE2),
                                  // shadowColor: themeColorBlue,
                                  elevation: 4,
                                  shadowColor: themeColorBlue,
                                  margin: const EdgeInsets.symmetric(
                                      horizontal: 12),
                                  shape: allcardBorder(),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      CircleAvatar(
                                        radius: 60,
                                        backgroundImage: photo != null
                                            ? NetworkImage(photo)
                                            : const NetworkImage(
                                                "https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8dXNlciUyMHByb2ZpbGV8ZW58MHx8MHx8&w=1000&q=80"),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Container(
                                        width: screensWidth * 0.5,
                                        margin: const EdgeInsets.symmetric(
                                          vertical: 5,
                                        ),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              name,
                                              style: txtStyle(16, Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              employee_code,
                                              style: txtStyle(16, Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              dept,
                                              style: txtStyle(12, Colors.white,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: setHeight(10),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 12),
                              child: loading
                                  ? Container(
                                      height: 200,
                                      child: const Center(
                                        child: CircularProgressIndicator(
                                          valueColor:
                                              AlwaysStoppedAnimation<Color>(
                                                  themeColorBlue),
                                        ),
                                      ),
                                    )
                                  : GridView.count(
                                      crossAxisCount: 4,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      shrinkWrap: true,
                                      mainAxisSpacing: 3,
                                      crossAxisSpacing: 3,
                                      // childAspectRatio: 50 / 50,
                                      primary: false,
                                      children: List.generate(
                                        gridObjectList.length,
                                        // testnames.length,
                                        (index) => GestureDetector(
                                          onTap: () {
                                            print(testroutes[index]);
                                            Navigator.pushNamed(
                                                context,
                                                //testroutes[index]
                                                gridObjectList[index]
                                                    ['linkname']);
                                          },
                                          child: Card(
                                            elevation: 4,
                                            shadowColor: themeColorBlue,
                                            // color: Colors.green,
                                            shape: allcardBorder(),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Icon(
                                                  testicons[index],
                                                  size: 35,
                                                ),
                                                SizedBox(
                                                  height: isCollapsed ? 10 : 2,
                                                ),
                                                Text(
                                                  gridObjectList[index]
                                                      ['displayname'],
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    fontSize: isCollapsed
                                                        ? setFontSize(12)
                                                        : setFontSize(8),
                                                    color: themeColorBlue,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                            ),
                            // Padding(
                            //   padding: const EdgeInsets.all(8.0),
                            //   child: InkWell(
                            //       child: Container(
                            //         height: setHeight(50),
                            //         margin: const EdgeInsets.symmetric(horizontal: 50),
                            //         decoration: BoxDecoration(
                            //           borderRadius: BorderRadius.circular(50),
                            //           color: themeColorBlue,
                            //         ),
                            //         child: Center(
                            //           child: Text(
                            //             "Leave",
                            //             style: txtStyle(14, Colors.white,
                            //                 fontWeight: FontWeight.bold),
                            //           ),
                            //         ),
                            //       ),
                            //       onTap: () {
                            //         Navigator.pushNamed(context, "/Leave");
                            //       }),
                            // ),
                            // Padding(
                            //   padding: const EdgeInsets.all(8.0),
                            //   child: InkWell(
                            //       child: Container(
                            //         height: setHeight(50),
                            //         margin: const EdgeInsets.symmetric(horizontal: 50),
                            //         decoration: BoxDecoration(
                            //           borderRadius: BorderRadius.circular(50),
                            //           color: themeColorBlue,
                            //         ),
                            //         child: Center(
                            //           child: Text(
                            //             "Change Pass",
                            //             style: txtStyle(14, Colors.white,
                            //                 fontWeight: FontWeight.bold),
                            //           ),
                            //         ),
                            //       ),
                            //       onTap: () {
                            //         Navigator.pushNamed(context, "/ChangePass");
                            //       }),
                            // ),
                            // Padding(
                            //   padding: const EdgeInsets.all(8.0),
                            //   child: InkWell(
                            //       child: Container(
                            //         height: setHeight(50),
                            //         margin: const EdgeInsets.symmetric(horizontal: 50),
                            //         decoration: BoxDecoration(
                            //           borderRadius: BorderRadius.circular(50),
                            //           color: themeColorBlue,
                            //         ),
                            //         child: Center(
                            //           child: Text(
                            //             "LOGOUT",
                            //             style: txtStyle(14, Colors.white,
                            //                 fontWeight: FontWeight.bold),
                            //           ),
                            //         ),
                            //       ),
                            //       onTap: () {
                            //         dropDetails();
                            //       }),
                            // ),
                            // SizedBox(),
                            // SizedBox(),
                            // SizedBox(),
                            // SizedBox(),
                            // SizedBox(),
                            // SizedBox(),
                            // SizedBox(),
                            footer(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
  }
}

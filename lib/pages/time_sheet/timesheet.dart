import 'dart:convert';

import 'package:epc_faculty/config/global.dart';
import 'package:epc_faculty/controller/apicall.dart';
import 'package:epc_faculty/pages/css/Colors.dart';
import 'package:epc_faculty/pages/css/textcss.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

var finalDate;
var tempdate;

class TimeSheet extends StatefulWidget {
  const TimeSheet({Key? key}) : super(key: key);

  @override
  _TimeSheetState createState() => _TimeSheetState();
}

class _TimeSheetState extends State<TimeSheet> {
  var tasklist = [];
  DateTime date = DateTime.now();

  @override
  initState() {
    getCurrentDate();
    getList();
    super.initState();
  }

  getCurrentDate() {
    setState(() {
      var now = DateTime.now();
      finalDate = DateFormat('dd-MM-yyyy').format(now);
    });
  }

  void getList() async {
    Map<String, dynamic> jsonmap = {
      "timesheet_date":
          "${DateFormat('E').format(date)} ${DateFormat('MMM').format(date)} ${date.day} 00:00:00 ${date.timeZoneName} ${date.year}",
      "timesheet_id": null
    };
    print(jsonmap);
    APICall.returnAPI("addTimesheet", context, jsonmap).then((apiHitData) {
      if (mounted) {
        setState(() {
          final body = jsonDecode(apiHitData);
          if (body['status'] == "200") {
            print("status == 200");
            tasklist = body["task_list"];
            print(tasklist);
          }
        });
      }
    });
  }

  Future pickDate(BuildContext context) async {
    final DateTime? nowdate = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(DateTime.now().month - 1),
      lastDate: DateTime.now(),
    );
    // DatePickerDialog(
    //   initialDate: DateTime.now(),
    //   firstDate: DateTime(DateTime.now().year - 5),
    //   lastDate: DateTime(DateTime.now().year + 5),
    // );
    setState(() {
      date = nowdate!;
      getList();
    });
  }

  String getText() {
    if (date == null) {
      return finalDate;
    } else {
      return '${DateFormat('E').format(date)} ${date.day}-${date.month}-${date.year}';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80,
        shape: cardBorder(),
        elevation: 4,
        backgroundColor: themeColorBlue,
        title: Text("Time Sheet",
            style: txtStyle(16, Colors.white, fontWeight: FontWeight.bold)),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          const SizedBox(
            height: 10,
          ),
          Container(
            width: screenWidth - 24,
            height: 80,
            child: Card(
              color: themeColorBlue,
              shape: allcardBorder(),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () {
                      print(finalDate);
                      pickDate(context);
                    },
                    child: Row(
                      children: [
                        const Icon(
                          Icons.calendar_today,
                          color: Colors.white,
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Text(
                          getText(),
                          style: txtStyle(16, Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          tasklist.isEmpty
              ? Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 40.0),
                    child: Text(
                      "No Data Found",
                      style: txtStyle(16, Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                )
              : cardtimeSheet(),
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        shape: allcardBorder(),
        tooltip: 'Add Time Sheet',
        backgroundColor: themeColorBlue,
        onPressed: () {
          Navigator.pushNamed(
            context,
            "/AddTimeSheet",
          );
        },
        icon: const Icon(Icons.add),
        label: Text(
          "Add Time Sheet",
          style: txtStyle(12, Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  Widget cardtimeSheet() {
    return Expanded(
      child: ListView.builder(
          padding: const EdgeInsets.all(8),
          itemCount: tasklist.length,
          itemBuilder: (context, int index) {
            return Card(
              margin: EdgeInsets.all(10.0),
              color: whiteColor,
              child: Container(
                width: screenWidth,
                padding: EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Date",
                          style: TextStyle(
                            color: textSecondaryColor,
                            fontSize: setFontSize(12),
                          ),
                        ),
                        Text(
                          tasklist[index]['date'].toString().substring(0, 10),
                          style: TextStyle(
                            color: btnTextColorBlue,
                            fontSize: setFontSize(12),
                          ),
                        ),
                      ],
                    ),
                    const Divider(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Start time : ${tasklist[index]['starttime'].toString()}",
                          style: TextStyle(
                            color: btnTextColorBlue,
                            fontSize: setFontSize(12),
                          ),
                        ),
                        Text(
                          "End time : ${tasklist[index]['endtime'].toString()}",
                          style: TextStyle(
                            color: btnTextColorBlue,
                            fontSize: setFontSize(12),
                          ),
                        ),
                      ],
                    ),
                    const Divider(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Title",
                          style: TextStyle(
                            color: textSecondaryColor,
                            fontSize: setFontSize(12),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Container(
                          width: screenWidth - 60,
                          child: Text(
                            tasklist[index]['name'].toString(),
                            style: TextStyle(
                              color: btnTextColorBlue,
                              fontSize: setFontSize(12),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const Divider(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Text(
                          "Description",
                          style: TextStyle(
                            color: textSecondaryColor,
                            fontSize: setFontSize(12),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Container(
                          width: screenWidth - 60,
                          child: Text(
                            tasklist[index]['description'].toString(),
                            style: TextStyle(
                              color: btnTextColorBlue,
                              fontSize: setFontSize(12),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }

  // timeSheetList() {
  //   return Expanded(
  //     child: ListView.builder(
  //         padding: const EdgeInsets.all(8),
  //         itemCount: tasklist.length,
  //         itemBuilder: (context, int index) {
  //           return Card(
  //             color: Colors.grey,
  //             shape: allcardBorder(),
  //             child: Padding(
  //               padding:
  //                   const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
  //               child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //                 crossAxisAlignment: CrossAxisAlignment.start,
  //                 mainAxisSize: MainAxisSize.max,
  //                 children: [
  //                   Text(
  //                     tasklist[index]['date'].toString().substring(0, 10),
  //                     style: txtStyle(16, Colors.white,
  //                         fontWeight: FontWeight.bold),
  //                   ),
  //                   Row(
  //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                     children: [
  //                       Text(
  //                         "Start time : " +
  //                             tasklist[index]['starttime'].toString(),
  //                         style: txtStyle(16, Colors.white,
  //                             fontWeight: FontWeight.bold),
  //                       ),
  //                       Text(
  //                         "End time : " + tasklist[index]['endtime'].toString(),
  //                         style: txtStyle(16, Colors.white,
  //                             fontWeight: FontWeight.bold),
  //                       ),
  //                     ],
  //                   ),
  //                   Text(
  //                     tasklist[index]['name'].toString(),
  //                     style: txtStyle(16, Colors.white,
  //                         fontWeight: FontWeight.bold),
  //                   ),
  //                   Text(
  //                     tasklist[index]['description'].toString(),
  //                     style: txtStyle(16, Colors.white,
  //                         fontWeight: FontWeight.bold),
  //                   ),
  //                 ],
  //               ),
  //             ),
  //           );
  //         }),
  //   );
  // }
}

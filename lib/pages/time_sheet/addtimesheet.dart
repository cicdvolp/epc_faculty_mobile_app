import 'dart:convert';

import 'package:epc_faculty/config/global.dart';
import 'package:epc_faculty/controller/apicall.dart';
import 'package:epc_faculty/pages/css/Colors.dart';
import 'package:epc_faculty/pages/css/common_ui.dart';
import 'package:epc_faculty/pages/css/textcss.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

TextEditingController DescriptionController = TextEditingController();
TextEditingController OtherController = TextEditingController();
var finalDate;
var tempdate;
var loading = false;
var loader = true;

class Addtimesheet extends StatefulWidget {
  const Addtimesheet({Key? key}) : super(key: key);

  @override
  _AddtimesheetState createState() => _AddtimesheetState();
}

class _AddtimesheetState extends State<Addtimesheet> {
  DateTime date = DateTime.now();
  TimeOfDay time = TimeOfDay.now();
  TimeOfDay startTime = TimeOfDay.now();
  TimeOfDay endTime = TimeOfDay.now();
  bool others = false;
  var TaskList = [];

  void getList() async {
    Map<String, dynamic> jsonmap = {
      "timesheet_date":
          "${DateFormat('E').format(date)} ${DateFormat('MMM').format(date)} ${date.day} ${date.hour}:${date.minute}:${date.second} ${date.timeZoneName} ${date.year}",
      "timesheet_id": null
    };
    print(jsonmap);
    APICall.returnAPI("addTimesheet", context, jsonmap).then((apiHitData) {
      if (mounted) {
        setState(() {
          final body = jsonDecode(apiHitData);
          if (body['status'] == "200") {
            print("status == 200");
            TaskList = body['inst_timesheet_master_list'];
            instData();
          } else {
            toastFunctioncolor(
                context, 'Error ! while loading dropdown data', Colors.red);
          }
        });
      }
    });
  }

  var dropdownItems = <String>[];
  String dropdownValue = '';
  void instData() {
    if (TaskList.isNotEmpty) {
      print("task list will be");
      for (var i = 0; i < TaskList.length; i++) {
        dropdownItems.add(TaskList[i]['name']);
        print(dropdownItems[i]);
      }
      dropdownValue = dropdownItems[0];
      setState(() {
        loader = false;
      });
    }
    if (TaskList.isEmpty) {
      setState(() {
        dropdownValue = '';
        loader = false;
      });
    }
  }

  void addTimeSheet(String taskname, String taskdesc) async {
    Map<String, dynamic> jsonmap = {
      "inst_task_master_id": 2,
      "date":
          "${DateFormat('E').format(date)} ${DateFormat('MMM').format(date)} ${date.day} 00:00:00 ${date.timeZoneName} ${date.year}",
      "timesheet_id": null,
      "starttime": '${startTime.hour}:${startTime.minute}',
      "endtime": '${endTime.hour}:${endTime.minute}',
      "task_name": taskname,
      "task_description": taskdesc
    };
    print("API CALLLLLLLLL");
    print(jsonmap);
    APICall.returnAPI("saveTimesheet", context, jsonmap).then((apiHitData) {
      if (mounted) {
        setState(() {
          DescriptionController.clear();
          OtherController.clear();
          loading = false;
          final body = jsonDecode(apiHitData);
          print(body);
          if (body['status'] == "200") {
            print("Status=200");
            toastFunctioncolor(context, body['msg'], Colors.green);
          } else {
            toastFunctioncolor(context, body['msg'], Colors.red);
          }
        });
      }
    });
  }

  @override
  void initState() {
    getList();
    startTime = TimeOfDay.now();
    endTime = TimeOfDay.now();
    super.initState();
  }

  Future pickdate(BuildContext context) async {
    final DateTime? nowdate = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(DateTime.now().day - 1),
      lastDate: DateTime.now(),
    );

    setState(() {
      date = nowdate!;
    });
  }

  Future startsTime(BuildContext context) async {
    startTime = (await showTimePicker(context: context, initialTime: time))!;
    setState(() {});
  }

  Future endsTime(BuildContext context) async {
    endTime = (await showTimePicker(context: context, initialTime: time))!;
    setState(() {});
  }

  String getText() {
    return '${DateFormat('E').format(date)} ${DateFormat('MMM').format(date)} ${date.day}-${date.month}-${date.year}';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80,
        shape: cardBorder(),
        elevation: 4,
        backgroundColor: themeColorBlue,
        title: Text(
          "Add Time Sheet",
          style: txtStyle(16, Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: ListView(
        children: [
          const SizedBox(
            height: 10,
          ),
          Center(
            child: Container(
              width: screenWidth - 24,
              height: 60,
              child: Card(
                color: themeColorBlue,
                shape: allcardBorder(),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () {
                        pickdate(context);
                      },
                      child: Row(
                        children: [
                          Icon(
                            Icons.calendar_today,
                            color: Colors.white,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            getText(),
                            style: txtStyle(14, Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(
                "Start Time",
                style: txtStyle(16, Colors.black, fontWeight: FontWeight.bold),
              ),
              Text(
                "End Time",
                style: txtStyle(16, Colors.black, fontWeight: FontWeight.bold),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                onTap: () {
                  TimePickerDialog(initialTime: time);
                },
                child: Container(
                  width: 150,
                  child: Card(
                    color: themeColorBlue,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            startTime.hour.toString() +
                                ":" +
                                startTime.minute.toString(),
                            style: txtStyle(14, Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                          GestureDetector(
                            onTap: () {
                              startsTime(context);
                            },
                            child: Icon(
                              Icons.timer,
                              color: Colors.white,
                            ),
                          )
                        ],
                      ),
                    ),
                    shape: allcardBorder(),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  TimePickerDialog(initialTime: time);
                },
                child: Container(
                  width: 150,
                  child: Card(
                    color: themeColorBlue,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                              endTime.hour.toString() +
                                  ":" +
                                  endTime.minute.toString(),
                              style: txtStyle(16, Colors.white,
                                  fontWeight: FontWeight.bold)),
                          GestureDetector(
                            onTap: () {
                              endsTime(context);
                            },
                            child: Icon(
                              Icons.timer,
                              color: Colors.white,
                            ),
                          )
                        ],
                      ),
                    ),
                    shape: allcardBorder(),
                  ),
                ),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12, left: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text("Task",
                    style: txtStyle(16, Colors.black,
                        fontWeight: FontWeight.bold)),
              ],
            ),
          ),
          Center(
            child: loader == true
                ? CircularProgressIndicator()
                : dropdownValue == ''
                    ? Text(
                        'Task Not Found',
                        style: txtStyle(16, Colors.black,
                            fontWeight: FontWeight.bold),
                      )
                    : Card(
                        color: Colors.white,
                        shape: allcardBorder(),
                        child: Container(
                          width: MediaQuery.of(context).size.width - 60,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 10),
                            child: DropdownButton<String>(
                              iconEnabledColor: Colors.black,
                              elevation: 16,
                              underline: const SizedBox(),
                              style: txtStyle(14, Colors.black,
                                  fontWeight: FontWeight.bold),
                              value: dropdownValue == null
                                  ? "Select Task"
                                  : dropdownValue,
                              hint: Text(
                                "Select Task",
                                style: txtStyle(14, Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                              onChanged: (String? newValue) {
                                setState(() {
                                  dropdownValue = newValue!;
                                  if (dropdownValue == "testg") {
                                    // toastFunctioncolor(
                                    //     context, dropdownValue, Colors.grey);
                                    others = true;
                                  } else {
                                    others = false;
                                  }
                                });
                              },
                              items: dropdownItems
                                  .map<DropdownMenuItem<String>>(
                                      (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ),
                          ),
                        ),
                      ),
          ),
          others == true
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 30, top: 10, bottom: 10),
                      child: Text("Other task",
                          style: txtStyle(16, Colors.black,
                              fontWeight: FontWeight.bold)),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: screenWidth - 60,
                            child: TextFormField(
                              controller: OtherController,
                              decoration: const InputDecoration(
                                hintText: "Task",
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                border: OutlineInputBorder(),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              : const SizedBox(),
          Padding(
            padding: const EdgeInsets.only(left: 30, top: 10, bottom: 10),
            child: Text("Description",
                style: txtStyle(16, Colors.black, fontWeight: FontWeight.bold)),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: screenWidth - 60,
                  child: TextFormField(
                    controller: DescriptionController,
                    decoration: const InputDecoration(
                      hintText: "Description",
                      hintStyle: TextStyle(
                        color: Colors.grey,
                      ),
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          InkWell(
              child: Container(
                height: setHeight(50),
                margin: const EdgeInsets.symmetric(horizontal: 50),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: themeColorBlue,
                ),
                child: Center(
                  child: Text(
                    "UPDATE TIME SHEET",
                    style:
                        txtStyle(14, Colors.white, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              onTap: () {
                if (startTime.toString() == endTime.toString()) {
                  toastFunctioncolor(
                      context, "Start time & End time are same", Colors.red);
                } else if (startTime.hour > endTime.hour) {
                  toastFunctioncolor(context,
                      "Start time is cannot less than End time", Colors.red);
                } else if (startTime.hour == endTime.hour &&
                    startTime.minute > endTime.minute) {
                  toastFunctioncolor(context,
                      "Start time is cannot less than End time", Colors.red);
                } else if (dropdownValue == "testg") {
                  if (OtherController.text.isNotEmpty) {
                    if (DescriptionController.text.isNotEmpty) {
                      loading = true;
                      addTimeSheet(
                        OtherController.text,
                        DescriptionController.text,
                      );
                      toastFunctioncolor(
                          context,
                          '${startTime.hour}:${startTime.minute} ${endTime.hour}:${endTime.minute} ${OtherController.text} ${DescriptionController.text}',
                          Colors.green);
                    } else {
                      toastFunctioncolor(
                          context, "Mention Description of Task", Colors.red);
                    }
                  } else {
                    toastFunctioncolor(
                        context, "Mention Other Task", Colors.red);
                  }
                } else if (DescriptionController.text.isEmpty) {
                  toastFunctioncolor(
                      context, "Mention Description of Task", Colors.red);
                } else {
                  loading = true;
                  addTimeSheet(
                    dropdownValue,
                    DescriptionController.text,
                  );
                  toastFunctioncolor(
                      context,
                      '${startTime.hour}:${startTime.minute}\n${endTime.hour}:${endTime.minute}\n${dropdownValue}\n${DescriptionController.text}',
                      Colors.green);
                }
              }),
        ],
      ),
    );
  }
}

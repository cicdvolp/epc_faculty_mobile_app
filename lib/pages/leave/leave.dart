import 'dart:convert';

import 'package:epc_faculty/controller/apicall.dart';
import 'package:epc_faculty/pages/css/Colors.dart';
import 'package:epc_faculty/pages/css/textcss.dart';
import 'package:flutter/material.dart';

class Leave extends StatefulWidget {
  const Leave({Key? key}) : super(key: key);

  @override
  _LeaveState createState() => _LeaveState();
}

class _LeaveState extends State<Leave> {
  var leavesData = [];
  var colors = [
    Colors.cyan,
    textPrimaryColor1,
    textSecondaryColor,
    themeColorYellow
  ];

  @override
  void initState() {
    getLeaveTasks();
    super.initState();
  }

  void getLeaveTasks() async {
    Map<String, dynamic> jsonmap = {};
    APICall.returnAPIWithHeader("getLeaves", context, jsonmap)
        .then((apiHitData) {
      if (mounted) {
        setState(() {
          final body = jsonDecode(apiHitData);
          if (body['status'] == "200") {
            leavesData = body['employeeleavebalancesheet'];
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80,
        shape: cardBorder(),
        elevation: 4,
        backgroundColor: themeColorBlue,
        title: Text(
          "Leave",
          style: txtStyle(16, Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: ListView.builder(
          padding: const EdgeInsets.all(8),
          itemCount: leavesData.length,
          itemBuilder: (context, int index) {
            return Container(
              // height: 150,
              child: Card(
                color: leavesData[index]['color'] ?? colors[index],
                shape: allcardBorder(),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 25.0, horizontal: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            leavesData[index]['leavetype']['type'],
                            style: txtStyle(16, Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "Balance : ${leavesData[index]['vacancy'].toString()}",
                            style: txtStyle(16, Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "Total : ${leavesData[index]['total'].toString()}",
                            style: txtStyle(16, Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "In Progress : ${leavesData[index]['inprocess'].toString()}",
                            style: txtStyle(16, Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            "Availed : ${leavesData[index]['approved'].toString()}",
                            style: txtStyle(16, Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }
}

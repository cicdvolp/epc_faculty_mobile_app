import 'package:epc_faculty/config/global.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';

double defaultScreenWidth = 400.0;
double defaultScreenHeight = 810.0;

TextStyle txtStyle(double size, var colorValue, {var fontWeight}) {
  var scaleWidth = 0.0;
  if (screenWidth > 400)
    scaleWidth = screenWidth / defaultScreenWidth;
  else
    scaleWidth = screenWidth / (screenWidth * 0.9);
  if (fontWeight != null) {
    return TextStyle(
      inherit: true,
      fontSize: (size * scaleWidth) / textScaleFactors,
      fontFamily: "Poppins",
      color: colorValue,
      fontWeight: fontWeight,
    );
  } else {
    return TextStyle(
      inherit: true,
      fontSize: (size * scaleWidth) / textScaleFactors,
      fontFamily: "Poppins",
      color: colorValue,
    );
  }
}

double setFontSize(double fontSize) {
  return setWidth(fontSize) / textScaleFactors;
}

double setFontSizewithcontex(double fontSize, BuildContext context) {
  textScaleFactors = MediaQuery.of(context).textScaleFactor;
  return setWidth(fontSize) / textScaleFactors;
}

double setWidth(double widthSize) {
  return widthSize * (screenWidth / defaultScreenWidth);
}

double setWidthwithcontex(double widthSize, BuildContext context) {
  screenWidth = MediaQuery.of(context).size.width;
  return widthSize * (screenWidth / defaultScreenWidth);
}

double setHeightwithcontex(double heightSize, BuildContext context) {
  screenHeight = MediaQuery.of(context).size.height;
  return heightSize * (screenHeight / defaultScreenHeight);
}

double setHeight(double heightSize) {
  return heightSize * (screenHeight / defaultScreenHeight);
}

ShapeBorder cardBorder() {
  return RoundedRectangleBorder(
    borderRadius: BorderRadius.vertical(
      bottom: Radius.circular(30),
    ),
  );
}

ShapeBorder allcardBorder() {
  return const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(20),
    ),
  );
}

import 'package:another_flushbar/flushbar.dart';
import 'package:epc_faculty/pages/css/Colors.dart';
import 'package:epc_faculty/pages/css/textcss.dart';
import 'package:flutter/material.dart';

Widget toastFunctioncolor(BuildContext context, String msg, Color col) {
  return Flushbar(
    backgroundColor: col,
    margin: EdgeInsets.all(20.0),
    borderRadius: BorderRadius.all(Radius.elliptical(8, 8)),
    flushbarStyle: FlushbarStyle.FLOATING,
    // message: msg,
    messageText: Text(
      msg,
      style: TextStyle(color: Colors.white, fontSize: 15.0),
    ),

    duration: Duration(seconds: 3),
  )..show(context);
}

Widget CircularImage() {
  return Container();
}

Widget cardView(Widget cardBody) {
  return Card(
    elevation: 4,
    shadowColor: themeColorBlue,
    margin: const EdgeInsets.symmetric(horizontal: 12),
    shape: allcardBorder(),
    child: cardBody,
  );
}

import 'package:epc_faculty/pages/css/Colors.dart';
import 'package:epc_faculty/pages/css/textcss.dart';
import 'package:epc_faculty/config/global.dart';
import 'package:flutter/material.dart';

class UnderConstruction extends StatefulWidget {
  const UnderConstruction({Key? key}) : super(key: key);

  @override
  _UnderConstructionState createState() => _UnderConstructionState();
}

class _UnderConstructionState extends State<UnderConstruction> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: const Image(
                image: AssetImage(
                  "assets/images/background/page_under_construction.jpg",
                ),
              ),
            ),
            SizedBox(
              height: setHeight(20),
            ),
            Text(
              "We're doing our best be back in",
              style: TextStyle(
                color: themeColorYellow,
                fontSize: setFontSize(22),
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: setHeight(20),
            ),
            Container(
              margin: EdgeInsets.only(
                  left: screenWidth / 3, right: screenWidth / 3),
              padding: const EdgeInsets.only(
                  left: 10, right: 10, top: 10, bottom: 10),
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.blue,
                  width: 2,
                ),
                borderRadius: BorderRadius.circular(12),
              ),
              child: Center(
                child: InkWell(
                  child: Text(
                    "GO HOME",
                    style: TextStyle(
                      color: Colors.blue[900],
                      fontWeight: FontWeight.bold,
                      fontSize: setFontSize(16),
                    ),
                  ),
                  onTap: () {
                    Navigator.pushNamed(
                      context,
                      "/HomePage",
                    );
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

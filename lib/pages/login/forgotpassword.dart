import 'dart:convert';

import 'package:epc_faculty/controller/apicall.dart';
import 'package:epc_faculty/pages/css/Colors.dart';
import 'package:epc_faculty/pages/css/common_ui.dart';
import 'package:epc_faculty/pages/css/textcss.dart';
import 'package:epc_faculty/pages/login/footer.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

TextEditingController emailController = TextEditingController();

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({Key? key}) : super(key: key);

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  void sendMail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("reset_email", emailController.text);
    Map<String, dynamic> params = {
      "email": emailController.text,
    };
    if (emailController.text.isEmpty) {
      toastFunctioncolor(context, "Email can not be empty", Colors.red);
    } else {
      APICall.returnPostAPIWithoutHeader("verifyemail", context, params)
          .then((apiHitData) {
        if (mounted) {
          setState(() {
            final body = json.decode(apiHitData);
            if (body["status"] == "200") {
              //toastFunctioncolor(context, body['msg'].toString(), Colors.red);
              Navigator.pushNamed(context, "/verifyOTP");
              toastFunctioncolor(context, body['msg'].toString(), Colors.green);
            } else {
              toastFunctioncolor(context, body['msg'].toString(), Colors.red);
            }
          });
        }
      });
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: themeColorBlue,
        elevation: 0,
      ),
      body: Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: setHeight(160),
              decoration: const BoxDecoration(
                color: themeColorBlue,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Center(
                          child: Image(
                            width: setWidth(200),
                            image: const AssetImage(
                              'assets/images/logo/edupluscampuswhite.png',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  // SizedBox(height: setHeight(50)),
                ],
              ),
            ),
            Expanded(
              child: Container(
                color: Colors.white,
                child: SingleChildScrollView(
                  child: Container(
                    height: MediaQuery.of(context).size.height - 250,
                    child: Padding(
                      padding:
                          const EdgeInsets.only(top: 30, right: 30, left: 30),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          const SizedBox(),
                          Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: const [
                                BoxShadow(
                                  color: login_header_color,
                                  blurRadius: 20,
                                  offset: Offset(0, 5),
                                )
                              ],
                            ),
                            child: Container(
                              padding: const EdgeInsets.all(10),
                              child: TextFormField(
                                controller: emailController,
                                decoration: const InputDecoration(
                                  hintText: "Email",
                                  hintStyle: TextStyle(
                                    color: Colors.grey,
                                  ),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                          InkWell(
                              child: Container(
                                height: setHeight(50),
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 50),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: themeColorBlue,
                                ),
                                child: Center(
                                  child: Text(
                                    "Proceed",
                                    style: txtStyle(14, Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                              onTap: () {
                                if (emailController.text.isEmpty) {
                                  toastFunctioncolor(context,
                                      "Email cannot be empty", Colors.red);
                                } else {
                                  bool emailValid = RegExp(
                                          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                      .hasMatch(emailController.text);
                                  if (emailValid == true) {
                                    sendMail();
                                  } else {
                                    toastFunctioncolor(context,
                                        "Invalid Email type", Colors.red);
                                  }
                                }
                              }),
                          const SizedBox(),
                          footer()
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'dart:convert';

import 'package:epc_faculty/controller/apicall.dart';
import 'package:epc_faculty/pages/css/Colors.dart';
import 'package:epc_faculty/pages/css/common_ui.dart';
import 'package:epc_faculty/pages/css/textcss.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'footer.dart';

TextEditingController passwordController = TextEditingController();
TextEditingController password1Controller = TextEditingController();

class CreatePassword extends StatefulWidget {
  const CreatePassword({Key? key}) : super(key: key);

  @override
  _CreatePasswordState createState() => _CreatePasswordState();
}

class _CreatePasswordState extends State<CreatePassword> {
  var loading = false;

  void changePassword() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var email = prefs.getString("reset_email");
    Map<String, dynamic> params = {
      "email": email,
      "newpassword": password1Controller.text,
      "confirmpassword": passwordController.text
    };
    if (password1Controller.text.isEmpty && passwordController.text.isEmpty) {
      toastFunctioncolor(
          context, "Password fields cannot be empty", Colors.red);
    } else if (password1Controller.text != passwordController.text) {
      toastFunctioncolor(
          context, "Password fields are not matched", Colors.red);
    } else {
      APICall.returnPostAPIWithoutHeader("resetForgotPass", context, params)
          .then((apiHitData) {
        if (mounted) {
          setState(() {
            final body = jsonDecode(apiHitData);
            if (body["msg"] == "200") {
              Map<String, dynamic> jsonmap = {
                "username": email,
                "password": passwordController.text,
              };
              APICall.returnGETAPIWithoutHeader("login", context, jsonmap)
                  .then((apiHitData) {
                if (mounted) {
                  setState(() {
                    final body = json.decode(apiHitData);
                    if (body['status'] == "pass") {
                      // loading = false;
                      prefs.clear();
                      prefs.setString("username", email!);
                      prefs.setString("password", passwordController.text);
                      prefs.setString("flname", body['instructor']['flname']);
                      prefs.setString("loginid", body['loginid'].toString());
                      prefs.setString(
                          "orgid", body['organization_id'].toString());
                      prefs.setString(
                          "profile_photo", body['instructor']['profilephoto']);
                      prefs.setString("dept_abbr",
                          body['department']['department_abbrivation'] ?? "-");
                      prefs.setString("dept_id",
                          body['department']['department_id'].toString());
                      prefs.setString(
                          "dept", body['department']['department'] ?? "-");
                      prefs.setString(
                          "cay_id", body['c_ay']['cay_id'].toString());
                      prefs.setString("cay", body['c_ay']['cay'] ?? "-");
                      prefs.setString(
                          "csem_id", body['c_sem']['csem_id'].toString());
                      prefs.setString("csem", body['c_sem']['csem'] ?? "-");
                      Navigator.pushNamed(
                        context,
                        "/HomePage",
                      );
                    } else {
                      toastFunctioncolor(context, body['msg'], Colors.red);
                    }
                  });
                }
              });

              toastFunctioncolor(
                  context, "Your Password successfully changed.", Colors.green);
            } else {
              toastFunctioncolor(context, body['msg'].toString(), Colors.red);
            }
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80,
        shape: cardBorder(),
        elevation: 4,
        backgroundColor: themeColorBlue,
        title: const Text("Create Password"),
      ),
      body: Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Container(
                height: MediaQuery.of(context).size.height,
                color: Colors.white,
                child: SingleChildScrollView(
                  child: Container(
                    height: MediaQuery.of(context).size.height - 250,
                    child: Padding(
                      padding:
                          const EdgeInsets.only(top: 30.0, left: 30, right: 30),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          const SizedBox(),
                          Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: const [
                                BoxShadow(
                                  color: login_header_color,
                                  blurRadius: 20,
                                  offset: Offset(0, 5),
                                )
                              ],
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  child: TextFormField(
                                    controller: passwordController,
                                    decoration: const InputDecoration(
                                      hintText: "New Password",
                                      hintStyle: TextStyle(
                                        color: Colors.grey,
                                      ),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  child: TextFormField(
                                    controller: password1Controller,
                                    obscureText: true,
                                    decoration: const InputDecoration(
                                      hintText: "Confirm Password",
                                      hintStyle: TextStyle(color: Colors.grey),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          loading
                              ? const CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      themeColorYellow),
                                )
                              : InkWell(
                                  child: Container(
                                    height: setHeight(50),
                                    margin: const EdgeInsets.symmetric(
                                        horizontal: 50),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      color: themeColorBlue,
                                    ),
                                    child: Center(
                                      child: Text(
                                        "RESET PASSWORD",
                                        style: txtStyle(14, Colors.white,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  onTap: () {
                                    FocusScope.of(context).unfocus();
                                    changePassword();
                                  }),
                          const SizedBox(),
                          const SizedBox(),
                          const SizedBox(),
                          footer(),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'dart:async';
import 'package:epc_faculty/config/global.dart';
import 'package:epc_faculty/pages/home/home.dart';
import 'package:epc_faculty/pages/leave/leave.dart';
import 'package:epc_faculty/pages/login/changepassword.dart';
import 'package:epc_faculty/pages/login/common.dart';
import 'package:epc_faculty/pages/login/createpassword.dart';
import 'package:epc_faculty/pages/login/forgotpassword.dart';
import 'package:epc_faculty/pages/login/login.dart';
import 'package:epc_faculty/pages/login/verifyotp.dart';
import 'package:epc_faculty/pages/other/underdev.dart';
import 'package:epc_faculty/pages/time_sheet/addtimesheet.dart';
import 'package:epc_faculty/pages/time_sheet/timesheet.dart';
import 'package:epc_faculty/pages/time_table/timetable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:epc_faculty/pages/css/AppTheme.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyAppStart extends StatelessWidget {
  const MyAppStart({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: buildTheme(),
      routes: <String, WidgetBuilder>{
        // Login Module
        '/': (BuildContext context) => Splash(), // Default home route
        '/Login': (BuildContext context) => Login(),
        '/HomePage': (BuildContext context) => HomePage(),
        '/ForgotPassword': (BuildContext context) => ForgotPassword(),
        "/verifyOTP": (BuildContext context) => VerifyOtp(),
        "/resetPass": (BuildContext context) => CreatePassword(),
        '/ChangePass': (BuildContext context) => ChangePassword(),

        // other
        '/underDevlopment': (BuildContext context) => UnderConstruction(),

        // Time Table Module
        '/Timetable': (BuildContext context) => TimeTable(),

        // Leaves Module
        '/Leave': (BuildContext context) => Leave(),

        // Time Sheet Module
        "/TimeSheet": (BuildContext context) => TimeSheet(),
        "/AddTimeSheet": (BuildContext context) => Addtimesheet(),
      },
    );
  }
}

class Splash extends StatefulWidget {
  @override
  SplashState createState() => new SplashState();
}

class SplashState extends State<Splash> with WidgetsBindingObserver {
  Future checkFirstSeen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool? _seen =
        (prefs.getBool('seen') == null) ? false : prefs.getBool('seen');

    prefs.setBool('seen', true);

    // String data;
    // var params = {
    // "uid": prefs.getString("username"),
    // "pass": prefs.getString("password"),
    // };
    // APICall.returnAPI("login", context, params).then((response) {
    // setState(() {
    //   data = response;
    // });
    // final body = json.decode(data);

    // if (body['msg'] == "200") {
    //   setState(() {
    //     imagepath = body['profilephoto'];
    //   });

    // print("alreay login.");
    // Navigator.pushNamed(
    //   context,
    //   "/MainDashboard",
    // );
    // } else {
    // Navigator.of(context).push(new MaterialPageRoute(
    //     builder: (context) => new Login(
    //           msg: body['msg'],
    //         )));

    Navigator.pushNamed(
      context,
      "/Login",
    );
    // }
    // });
    prefs.setBool('seen', true);
  }

  void checklogin() {}

  getRemoteConfig() async {}

  @override
  void initState() {
    super.initState();
    checkFirstSeen();
    // this.initDynamicLinks();
    WidgetsBinding.instance!.addObserver(this);
    Timer(const Duration(milliseconds: 20), () {
      var sWidth = MediaQuery.of(context).size.width;
      var sHeight = MediaQuery.of(context).size.height;
      textScaleFactors = MediaQuery.of(context).textScaleFactor;
      screenHeight = sHeight;
      screenWidth = sWidth;
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeights = MediaQuery.of(context).size.height;
    screenHeight = screenHeights;
    // GlobalAnalytics.sendAnalytics("AppLaunch");
    //checkAccessToken();
    return WillPopScope(
      onWillPop: () => onWillPop(context, onWillPop: () {}),
      child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[],
        ),
      ),
    );
  }
}

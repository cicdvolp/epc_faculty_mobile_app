import 'dart:convert';
import 'package:epc_faculty/controller/apicall.dart';
import 'package:epc_faculty/pages/css/Colors.dart';
import 'package:epc_faculty/pages/css/common_ui.dart';
import 'package:epc_faculty/pages/css/textcss.dart';
import 'package:epc_faculty/pages/login/footer.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

bool buttonpressed = false;
TextEditingController emailController = TextEditingController();
TextEditingController passwordController = TextEditingController();

class Login extends StatefulWidget {
  final String? msg;
  const Login({Key? key, this.msg}) : super(key: key);
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Login>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  var loading = false;
  void initState() {
    checkLogin();
    loading = false;
    buttonpressed = false;
    if (widget.msg != null) {
      Future<Null>.delayed(Duration.zero, () {
        setState(() {
          scaffoldKey.currentState?.showSnackBar(
            SnackBar(
              content: Text(widget.msg!),
              duration: const Duration(seconds: 3),
            ),
          );
          //Navigator.pop(context);
        });
      });
    }
    super.initState();
    // CheckLoginController.checkLogin();
  }

  void checkLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool? loggedin = prefs.getBool('loggedin');
    if (loggedin == true) {
      Navigator.pushNamed(
        context,
        "/HomePage",
      );
    }
  }

  void doLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, dynamic> jsonmap = {
      "username": emailController.text,
      "password": passwordController.text,
    };
    APICall.returnGETAPIWithoutHeader("login", context, jsonmap)
        .then((apiHitData) {
      if (mounted) {
        setState(() {
          final body = json.decode(apiHitData);
          if (body['status'] == "pass") {
            prefs.clear();
            prefs.setString("username", emailController.text);
            prefs.setString("password", passwordController.text);
            prefs.setString("flname", body['instructor']['flname'].toString());
            prefs.setString("loginid", body['loginid'].toString());
            prefs.setString("emp_code", body['instructor']['code'].toString());
            prefs.setString("orgid", body['organization_id'].toString());
            prefs.setString(
                "profile_photo", body['instructor']['profilephoto']);
            prefs.setString("dept_abbr",
                body['department']['department_abbrivation'] ?? "-");
            prefs.setString(
                "dept_id", body['department']['department_id'].toString());
            prefs.setString("dept", body['department']['department'] ?? "-");
            prefs.setString("cay_id", body['c_ay']['cay_id'].toString());
            prefs.setString("cay", body['c_ay']['cay'] ?? "-");
            prefs.setString("csem_id", body['c_sem']['csem_id'].toString());
            prefs.setString("csem", body['c_sem']['csem'] ?? "-");
            loading = false;
            Navigator.pushNamed(
              context,
              "/HomePage",
            );
          } else {
            toastFunctioncolor(context, body['msg'], Colors.red);
            setState(() {
              loading = false;
            });
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: setHeight(250),
              decoration: const BoxDecoration(
                color: themeColorBlue,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: setHeight(50),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Center(
                          child: Image(
                            width: setWidth(200),
                            image: const AssetImage(
                              'assets/images/logo/edupluscampuswhite.png',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                height: MediaQuery.of(context).size.height - 250,
                color: Colors.white,
                child: SingleChildScrollView(
                  child: Container(
                    height: MediaQuery.of(context).size.height - 250,
                    child: Padding(
                      padding:
                          const EdgeInsets.only(top: 30.0, left: 30, right: 30),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          const SizedBox(),
                          Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: const [
                                BoxShadow(
                                  color: login_header_color,
                                  blurRadius: 20,
                                  offset: Offset(0, 5),
                                )
                              ],
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  child: TextFormField(
                                    controller: emailController,
                                    decoration: const InputDecoration(
                                      hintText: "Email or Phone number",
                                      hintStyle: TextStyle(
                                        color: Colors.grey,
                                      ),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.all(10),
                                  child: TextFormField(
                                    controller: passwordController,
                                    obscureText: true,
                                    decoration: const InputDecoration(
                                      hintText: "Password",
                                      hintStyle: TextStyle(color: Colors.grey),
                                      border: InputBorder.none,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              InkWell(
                                child: const Text(
                                  "Forgot Password?",
                                  style: TextStyle(color: Colors.grey),
                                ),
                                onTap: () {
                                  Navigator.pushNamed(
                                    context,
                                    "/ForgotPassword",
                                  );
                                },
                              ),
                            ],
                          ),
                          loading == true
                              ? const CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      themeColorYellow),
                                )
                              : InkWell(
                                  child: Container(
                                    height: setHeight(50),
                                    margin: const EdgeInsets.symmetric(
                                        horizontal: 50),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      color: themeColorBlue,
                                    ),
                                    child: Center(
                                      child: Text(
                                        "LOGIN",
                                        style: txtStyle(14, Colors.white,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      loading = true;
                                      buttonpressed = true;
                                    });
                                    FocusScope.of(context).unfocus();
                                    if (emailController.text
                                            .toString()
                                            .isNotEmpty &&
                                        passwordController.text
                                            .toString()
                                            .isNotEmpty) {
                                      bool emailValid = RegExp(
                                              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                          .hasMatch(emailController.text);
                                      if (emailValid == true) {
                                        doLogin();
                                      } else {
                                        toastFunctioncolor(
                                            context,
                                            "Please Enter Correct Email.",
                                            Colors.red);
                                        setState(() {
                                          loading = false;
                                        });
                                      }
                                    } else {
                                      toastFunctioncolor(
                                          context,
                                          "Username & password cannot be empty",
                                          Colors.red);
                                      setState(() {
                                        loading = false;
                                      });
                                    }
                                  }),
                          const SizedBox(),
                          const SizedBox(),
                          const SizedBox(),
                          footer(),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, size.height);
    path.quadraticBezierTo(
        size.width / 4, size.height - 60, size.width / 2, size.height - 30);
    path.quadraticBezierTo(
        3 / 4 * size.width, size.height, size.width, size.height - 30);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

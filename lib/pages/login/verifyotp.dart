import 'dart:convert';

import 'package:epc_faculty/controller/apicall.dart';
import 'package:epc_faculty/pages/css/Colors.dart';
import 'package:epc_faculty/pages/css/common_ui.dart';
import 'package:epc_faculty/pages/css/textcss.dart';
import 'package:epc_faculty/pages/login/footer.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

TextEditingController otpController = TextEditingController();

class VerifyOtp extends StatefulWidget {
  const VerifyOtp({Key? key}) : super(key: key);

  @override
  _VerifyOtpState createState() => _VerifyOtpState();
}

class _VerifyOtpState extends State<VerifyOtp> {
  void verifyOTP() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var email = prefs.getString("reset_email");
    Map<String, dynamic> params = {
      "otp": otpController.text,
      "email": email,
    };
    if (otpController.text.isEmpty) {
      toastFunctioncolor(context, "OTP can not be empty", Colors.red);
    } else {
      APICall.returnPostAPIWithoutHeader("verifyOTP", context, params)
          .then((apiHitData) {
        if (mounted) {
          setState(() {
            final body = json.decode(apiHitData);
            if (body["msg"] == "200") {
              Navigator.pushNamed(context, "/resetPass");
              toastFunctioncolor(
                  context, "OTP verified successfully", Colors.green);
            } else {
              toastFunctioncolor(
                  context, 'Please enter correct OTP', Colors.red);
            }
          });
        }
      });
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: themeColorBlue,
        elevation: 0,
      ),
      body: Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              height: setHeight(160),
              decoration: const BoxDecoration(
                color: themeColorBlue,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Center(
                          child: Image(
                            width: setWidth(200),
                            image: const AssetImage(
                              'assets/images/logo/edupluscampuswhite.png',
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  // SizedBox(height: setHeight(50)),
                ],
              ),
            ),
            Expanded(
              child: Container(
                color: Colors.white,
                child: SingleChildScrollView(
                  child: Container(
                    height: MediaQuery.of(context).size.height - 250,
                    child: Padding(
                      padding:
                          const EdgeInsets.only(top: 30, right: 30, left: 30),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          const SizedBox(),
                          Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: const [
                                BoxShadow(
                                  color: login_header_color,
                                  blurRadius: 20,
                                  offset: Offset(0, 5),
                                )
                              ],
                            ),
                            child: Container(
                              padding: const EdgeInsets.all(10),
                              child: TextFormField(
                                controller: otpController,
                                decoration: const InputDecoration(
                                  hintText: "OTP",
                                  hintStyle: TextStyle(
                                    color: Colors.grey,
                                  ),
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                          InkWell(
                              child: Container(
                                height: setHeight(50),
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 50),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: themeColorBlue,
                                ),
                                child: Center(
                                  child: Text(
                                    "Proceed",
                                    style: txtStyle(14, Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                              onTap: () {
                                verifyOTP();
                              }),
                          const SizedBox(),
                          footer()
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

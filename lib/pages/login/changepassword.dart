import 'dart:convert';
import 'package:epc_faculty/controller/apicall.dart';
import 'package:epc_faculty/pages/css/Colors.dart';
import 'package:epc_faculty/pages/css/common_ui.dart';
import 'package:epc_faculty/pages/css/textcss.dart';
import 'package:epc_faculty/pages/login/footer.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

TextEditingController newPassController = TextEditingController();
TextEditingController confirmPassController = TextEditingController();
TextEditingController currentPassController = TextEditingController();

class ChangePassword extends StatefulWidget {
  const ChangePassword({Key? key}) : super(key: key);

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  void changePassword() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, dynamic> params = {
      "newpassword": newPassController.text,
      "confirmpassword": confirmPassController.text,
      "email": prefs.getString("username"),
      "oldpassword": currentPassController.text
    };
    print("params:");
    print(params);
    APICall.returnPostAPIWithoutHeader("changePass", context, params)
        .then((apiHitData) {
      if (this.mounted) {
        final body = jsonDecode(apiHitData);
        print("body:");
        print(body);
        setState(() {
          print(json);
          if (body['msg'] == "200") {
            toastFunctioncolor(
                context, "Password changed successfully", Colors.green);
          } else {
            toastFunctioncolor(context, body['msg'], Colors.red);
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80,
        shape: cardBorder(),
        elevation: 4,
        backgroundColor: themeColorBlue,
        title: const Text("Change Password"),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const SizedBox(),
              Padding(
                padding: const EdgeInsets.all(30.0),
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: const [
                          BoxShadow(
                            color: login_header_color,
                            blurRadius: 20,
                            offset: Offset(0, 5),
                          )
                        ],
                      ),
                      child: Column(
                        children: <Widget>[
                          Container(
                            padding: const EdgeInsets.all(10),
                            child: TextFormField(
                              obscureText: true,
                              controller: currentPassController,
                              decoration: const InputDecoration(
                                hintText: "Current Password",
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.all(10),
                            child: TextFormField(
                              obscureText: true,
                              controller: newPassController,
                              decoration: const InputDecoration(
                                hintText: "New Password",
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.all(10),
                            child: TextFormField(
                              obscureText: true,
                              controller: confirmPassController,
                              decoration: const InputDecoration(
                                hintText: "Confirm Password",
                                hintStyle: TextStyle(
                                  color: Colors.grey,
                                ),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: InkWell(
                    child: Container(
                      height: setHeight(50),
                      margin: const EdgeInsets.symmetric(horizontal: 50),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: themeColorBlue,
                      ),
                      child: Center(
                        child: Text(
                          "Change Password",
                          style: txtStyle(14, Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    onTap: () {
                      print("func Calllll");
                      if (newPassController.text.isEmpty ||
                          confirmPassController.text.isEmpty ||
                          currentPassController.text.isEmpty) {
                        toastFunctioncolor(
                            context, "Passwords cannot be empty", Colors.red);
                        setState(() {
                          newPassController.clear();
                          confirmPassController.clear();
                          currentPassController.clear();
                        });
                      }
                      if (newPassController.text ==
                          confirmPassController.text) {
                        toastFunctioncolor(
                            context, "New Password Doesnt match !", Colors.red);
                        setState(() {
                          confirmPassController.clear();
                          currentPassController.clear();
                        });
                      } else {
                        changePassword();
                      }
                    }),
              ),
              const SizedBox(),
              const SizedBox(),
              const SizedBox(),
              footer(),
            ],
          ),
        ),
      ),
    );
  }
}

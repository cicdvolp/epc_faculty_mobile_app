import 'package:epc_faculty/pages/css/textcss.dart';
import 'package:epc_faculty/pages/css/Colors.dart';
import 'package:flutter/material.dart';

class EpnImages extends StatelessWidget {
  const EpnImages({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Image(
        width: setWidth(300),
        image: const AssetImage(
          'assets/images/logo/edupluscampus.png',
        ),
      ),
    );
  }
}

Future<bool> onWillPop(BuildContext context,
    {required Function() onWillPop}) async {
  return true;
}

showProgress(BuildContext context) {
  showDialog(
      context: context,
      builder: (BuildContext context) {
        return const Center(
          child: CircularProgressIndicator(
              //backgroundColor: whiteColor,
              valueColor: AlwaysStoppedAnimation<Color>(themeColorYellow)),
        );
      });
}

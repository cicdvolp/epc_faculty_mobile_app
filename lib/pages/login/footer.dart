import 'package:flutter/material.dart';

footer() {
  return Padding(
    padding: const EdgeInsets.all(8),
    child: Column(
      children: const [
        Image(
          width: 80,
          image: AssetImage(
            'assets/images/logo/edupluscampus.png',
          ),
        ),
        Text("Powered By EduplusCampus:VGESPL"),
      ],
    ),
  );
}

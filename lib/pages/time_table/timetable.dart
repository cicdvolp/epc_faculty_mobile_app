import 'dart:convert';

import 'package:epc_faculty/config/global.dart';
import 'package:epc_faculty/controller/apicall.dart';
import 'package:epc_faculty/pages/css/Colors.dart';
import 'package:epc_faculty/pages/css/common_ui.dart';
import 'package:epc_faculty/pages/css/textcss.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class TimeTable extends StatefulWidget {
  @override
  _TimeTableState createState() => _TimeTableState();
}

class _TimeTableState extends State<TimeTable> {
  TextEditingController urlController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  var isCollapsed = true;
  var dataloaded = false;
  var daylength = 0;
  var daylist = [];
  var ttlist = [];

  @override
  void initState() {
    getTimetable();
    super.initState();
  }

  void getTimetable() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var params = {
      "ay": prefs.getString("cay_id").toString(),
      "sem": prefs.getString("csem_id").toString(),
      "department": prefs.getString("dept_id").toString(),
      "slotmaster": null
    };
    APICall.returnAPI("getTimetable", context, params).then((apiHitData) {
      if (mounted) {
        setState(() {
          final body = jsonDecode(apiHitData);
          if (body['status'] == "200") {
            daylist = body['body']['days'];
            ttlist = body['body']['timetable'];
            setState(() {
              dataloaded = true;
            });
            for (var d in daylist) {
              daylength = daylength + 1;
            }
          }
        });
      }
    });
  }

  void upDateSessionURL(var ttId, var url, var ttVersionId) {
    Navigator.of(context).pop();
    print('url');
    print(url);
    bool _validURL = Uri.parse(url).isAbsolute;
    print('_validURL');
    print(_validURL);
    if (!_validURL) {
      print('false ---- ');
      toastFunctioncolor(
          context, "Please enter valid session link url", Colors.red);
    } else {
      var params = {
        "tt_version_id": ttVersionId,
        "timetableid": ttId,
        "online_class_url": url
      };
      print('params');
      print(params);
      APICall.returnAPI("sessionurl", context, params).then((apiHitData) {
        if (mounted) {
          setState(() {
            final body = jsonDecode(apiHitData);
            print(body);
            if (body['status'] == "200") {
              print(body);
              toastFunctioncolor(context, body['message'], Colors.green);
              // setState(() {});
            } else {
              toastFunctioncolor(context, body['message'], Colors.red);
            }
          });
        }
      });
    }
  }

  List<Widget> tabNameList() {
    List<Widget> list = [];
    for (var d in daylist) {
      list.add(Tab(text: d.toUpperCase()));
    }
    return list;
  }

  List<Widget> tabsList() {
    List<Widget> list = [];
    for (var d in daylist) {
      if (d.toString() == 'Sun') {
        list.add(weekend());
      } else {
        for (var tt in ttlist) {
          if (d.toString() == tt['day_short_name']) {
            var timetablelist = tt['timetablelist'];
            list.add(timeTableCardList(timetablelist, tt['day'].toString()));
          }
        }
      }
    }
    return list;
  }

  Widget timeTableCardList(List timetablelist, var day) {
    List<Widget> list = [];
    for (var tt in timetablelist) {
      Widget tt_card = Card(
        elevation: 4,
        shadowColor: themeColorBlue,
        margin: const EdgeInsets.symmetric(horizontal: 12, vertical: 5),
        shape: allcardBorder(),
        // child: Container(
        //   width: screenWidth,
        //   padding: const EdgeInsets.all(10.0),
        //   child: Column(
        //     mainAxisAlignment: MainAxisAlignment.start,
        //     crossAxisAlignment: CrossAxisAlignment.start,
        //     children: [
        //       Row(
        //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //         children: [
        //           Row(
        //             children: [
        //               Icon(
        //                 Icons.access_time_sharp,
        //                 color: Colors.black,
        //                 size: setFontSize(18),
        //               ),
        //               const SizedBox(
        //                 width: 10,
        //               ),
        //               Text(
        //                 tt['slot'].toString(),
        //                 style: TextStyle(
        //                   color: btnTextColorBlue,
        //                   fontSize: setFontSize(14),
        //                 ),
        //               ),
        //             ],
        //           ),
        //           Row(
        //             children: [
        //               ClipOval(
        //                 child: Material(
        //                   color: Colors.orange, // button color
        //                   child: InkWell(
        //                     //splashColor: Colors.red, // inkwell color
        //                     child: SizedBox(
        //                       width: 25,
        //                       height: 25,
        //                       child: Icon(
        //                         Icons.edit,
        //                         color: Colors.white,
        //                         size: setFontSize(14),
        //                       ),
        //                     ),
        //                     onTap: () {
        //                       setState(() {});
        //                     },
        //                   ),
        //                 ),
        //               ),
        //               const SizedBox(
        //                 width: 10,
        //               ),
        //               ClipOval(
        //                 child: Material(
        //                   color: Colors.black, // button color
        //                   child: InkWell(
        //                     //splashColor: Colors.red, // inkwell color
        //                     child: SizedBox(
        //                       width: 25,
        //                       height: 25,
        //                       child: Icon(
        //                         Icons.play_arrow_sharp,
        //                         color: Colors.white,
        //                         size: setFontSize(14),
        //                       ),
        //                     ),
        //                     onTap: () {
        //                       setState(() {});
        //                     },
        //                   ),
        //                 ),
        //               ),
        //             ],
        //           ),
        //         ],
        //       ),
        //       const SizedBox(
        //         height: 8,
        //       ),
        //       Text(
        //         tt['course_code'].toString() +
        //             " : " +
        //             tt['course_name'].toString(),
        //         style: TextStyle(
        //           color: btnTextColorBlue,
        //           fontSize: setFontSize(12),
        //         ),
        //       ),
        //       const SizedBox(
        //         height: 8,
        //       ),
        //       Row(
        //         children: [
        //           tt['year'] == null
        //               ? const Text('')
        //               : Text(
        //                   tt['year'].toString(),
        //                   style: TextStyle(
        //                     color: btnTextColorBlue,
        //                     fontSize: setFontSize(12),
        //                   ),
        //                 ),
        //           const SizedBox(
        //             width: 10,
        //           ),
        //           tt['division'] == null
        //               ? const Text('')
        //               : Text(
        //                   tt['division'].toString(),
        //                   style: TextStyle(
        //                     color: btnTextColorBlue,
        //                     fontSize: setFontSize(12),
        //                   ),
        //                 ),
        //           const SizedBox(
        //             width: 10,
        //           ),
        //           tt['batch_display_name'] == null
        //               ? const Text('')
        //               : Text(
        //                   tt['batch_display_name'].toString(),
        //                   style: TextStyle(
        //                     color: btnTextColorBlue,
        //                     fontSize: setFontSize(12),
        //                   ),
        //                 ),
        //         ],
        //       ),
        //       const SizedBox(
        //         height: 8,
        //       ),
        //       Row(
        //         children: [
        //           tt['room_no'] == null
        //               ? const Text('')
        //               : Text(
        //                   tt['room_no'],
        //                   style: TextStyle(
        //                     color: btnTextColorBlue,
        //                     fontSize: setFontSize(12),
        //                   ),
        //                 ),
        //           const SizedBox(
        //             width: 10,
        //           ),
        //           Text(
        //             tt['load_type'].toString(),
        //             style: TextStyle(
        //               color: btnTextColorBlue,
        //               fontSize: setFontSize(12),
        //             ),
        //           ),
        //           const SizedBox(
        //             width: 10,
        //           ),
        //         ],
        //       ),
        //       const SizedBox(
        //         height: 8,
        //       ),
        //       Text(
        //         tt['program'].toString(),
        //         style: TextStyle(
        //           color: btnTextColorBlue,
        //           fontSize: setFontSize(12),
        //         ),
        //       ),
        //     ],
        //   ),
        // ),
        child: Container(
          width: screenWidth,
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Slot",
                    style: TextStyle(
                      color: textSecondaryColor,
                      fontSize: setFontSize(12),
                    ),
                  ),
                  Text(
                    tt['slot'].toString(),
                    style: TextStyle(
                      color: btnTextColorBlue,
                      fontSize: setFontSize(12),
                    ),
                  ),
                ],
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Year : Division",
                    style: TextStyle(
                      color: textSecondaryColor,
                      fontSize: setFontSize(12),
                    ),
                  ),
                  Text(
                    tt['year'].toString() + ": " + tt['division'].toString(),
                    style: TextStyle(
                      color: btnTextColorBlue,
                      fontSize: setFontSize(12),
                    ),
                  ),
                ],
              ),
              tt['room_no'] == null
                  ? Container()
                  : Column(
                      children: [
                        Divider(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Room No.",
                              style: TextStyle(
                                color: textSecondaryColor,
                                fontSize: setFontSize(12),
                              ),
                            ),
                            Text(
                              tt['room_no'].toString(),
                              style: TextStyle(
                                color: btnTextColorBlue,
                                fontSize: setFontSize(12),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Course : Batch No.",
                    style: TextStyle(
                      color: textSecondaryColor,
                      fontSize: setFontSize(12),
                    ),
                  ),
                  tt['batch_display_name'] == null
                      ? Text(
                          tt['course_code'].toString() +
                              " : " +
                              tt['course_name_abbrivation'].toString(),
                          // " : " +
                          // tt['batch_display_name'].toString(),
                          style: TextStyle(
                            color: btnTextColorBlue,
                            fontSize: setFontSize(12),
                          ),
                        )
                      : Text(
                          tt['course_code'].toString() +
                              " : " +
                              tt['course_name_abbrivation'].toString() +
                              " : " +
                              tt['batch_display_name'].toString(),
                          style: TextStyle(
                            color: btnTextColorBlue,
                            fontSize: setFontSize(12),
                          ),
                        ),
                ],
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Load Type",
                    style: TextStyle(
                      color: textSecondaryColor,
                      fontSize: setFontSize(12),
                    ),
                  ),
                  Text(
                    tt['load_type'].toString(),
                    style: TextStyle(
                      color: btnTextColorBlue,
                      fontSize: setFontSize(12),
                    ),
                  ),
                ],
              ),
              tt['isonlinesession'] ? const Divider() : Container(),
              tt['isonlinesession']
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Online Lecture",
                          style: TextStyle(
                            color: textSecondaryColor,
                            fontSize: setFontSize(12),
                          ),
                        ),
                        Row(
                          textDirection: TextDirection.rtl,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            ClipOval(
                              child: Material(
                                color: Colors.lightGreen, // button color
                                child: InkWell(
                                  //splashColor: Colors.red, // inkwell color
                                  child: SizedBox(
                                    width: 25,
                                    height: 25,
                                    child: Icon(
                                      Icons.edit,
                                      color: Colors.white,
                                      size: setFontSize(14),
                                    ),
                                  ),
                                  onTap: () {
                                    setState(() {});

                                    showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return Opacity(
                                            opacity: 0.90,
                                            child: AlertDialog(
                                              shape: allcardBorder(),
                                              content: Stack(
                                                overflow: Overflow.visible,
                                                children: <Widget>[
                                                  // Positioned(
                                                  //   right: -15.0,
                                                  //   top: -15.0,
                                                  //   child: InkResponse(
                                                  //     onTap: () {
                                                  //       Navigator.of(context)
                                                  //           .pop();
                                                  //     },
                                                  //     child: const Icon(
                                                  //       Icons.close,
                                                  //       color: Colors.red,
                                                  //       size: 20.0,
                                                  //     ),
                                                  // child: const CircleAvatar(
                                                  //   radius: 15.0,
                                                  //   // maxRadius: 50,
                                                  //   // minRadius: 50,
                                                  //   child: Icon(
                                                  //     Icons.close,
                                                  //     size: 20.0,
                                                  //   ),
                                                  //   backgroundColor:
                                                  //       Colors.red,
                                                  // ),
                                                  //   ),
                                                  // ),

                                                  Form(
                                                    key: _formKey,
                                                    child: Column(
                                                      mainAxisSize:
                                                          MainAxisSize.min,
                                                      children: <Widget>[
                                                        // Text(tt['timetable_id']
                                                        //     .toString()),
                                                        // Text(
                                                        //     tt['timetable_version_id']
                                                        //         .toString()),
                                                        Container(
                                                          decoration:
                                                              BoxDecoration(
                                                            color: Colors.white,
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        10),
                                                            boxShadow: const [
                                                              BoxShadow(
                                                                color:
                                                                    login_header_color,
                                                                blurRadius: 20,
                                                                offset: Offset(
                                                                    0, 5),
                                                              )
                                                            ],
                                                          ),
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(10),
                                                          child: TextFormField(
                                                            controller:
                                                                urlController,
                                                            decoration:
                                                                const InputDecoration(
                                                              hintText:
                                                                  "Enter Online Session URL",
                                                              hintStyle:
                                                                  TextStyle(
                                                                color:
                                                                    Colors.grey,
                                                              ),
                                                              border:
                                                                  InputBorder
                                                                      .none,
                                                            ),
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          height: 12,
                                                        ),
                                                        Text(
                                                          'Sample URL - https://meet.google.com/xzn-vxwd-oqz',
                                                          style: txtStyle(12,
                                                              themeColorYellow,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w300),
                                                        ),
                                                        const SizedBox(
                                                          height: 12,
                                                        ),
                                                        InkWell(
                                                            child: Container(
                                                              height:
                                                                  setHeight(50),
                                                              margin: const EdgeInsets
                                                                      .symmetric(
                                                                  horizontal:
                                                                      50),
                                                              decoration:
                                                                  BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            50),
                                                                color:
                                                                    themeColorBlue,
                                                              ),
                                                              child: Center(
                                                                child: Text(
                                                                  "UPDATE",
                                                                  style: txtStyle(
                                                                      14,
                                                                      Colors
                                                                          .white,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold),
                                                                ),
                                                              ),
                                                            ),
                                                            onTap: () {
                                                              upDateSessionURL(
                                                                  tt[
                                                                      'timetable_id'],
                                                                  urlController
                                                                      .text,
                                                                  tt['timetable_version_id']);
                                                              print(
                                                                  'after call');
                                                              Navigator.of(
                                                                      context)
                                                                  .pop();
                                                            }),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          );
                                        });
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              width: setWidth(10),
                            ),
                            tt['onlinesesionurl'] == null
                                ? Container()
                                : ClipOval(
                                    child: Material(
                                      color: themeColorYellow, // button color
                                      child: InkWell(
                                        //splashColor: Colors.red, // inkwell color
                                        child: SizedBox(
                                          width: 25,
                                          height: 25,
                                          child: Icon(
                                            Icons.play_arrow_sharp,
                                            color: Colors.white,
                                            size: setFontSize(14),
                                          ),
                                        ),
                                        onTap: () {
                                          print("ABC----");
                                          print(tt['onlinesesionurl']);
                                          _launchURL(tt['onlinesesionurl']);
                                          setState(() {});
                                        },
                                      ),
                                    ),
                                  ),
                          ],
                        ),
                      ],
                    )
                  : Container(),
            ],
          ),
        ),
      );
      list.add(tt_card);
      list.add(
        const SizedBox(
          width: 10,
        ),
      );
    }
    if (list.length == 0) {
      return noLectureForToday(day);
    } else {
      return ListView(
        children: list,
      );
    }
    // return list;
  }

  Widget noLectureForToday(var day) {
    return Center(
      child: Container(
        padding: const EdgeInsets.all(0.0),
        margin: const EdgeInsets.symmetric(vertical: 5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // Container(
            //   margin: const EdgeInsets.fromLTRB(30, 0, 30, 0),
            //   child: const Image(
            //     image: AssetImage(
            //       "assets/images/background/page_under_construction.jpg",
            //     ),
            //   ),
            // ),
            // SizedBox(
            //   height: setHeight(20),
            // ),
            Text(
              "No Lectures allotted to you for " + day.toString(),
              style:
                  txtStyle(20, themeColorYellow, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            // SizedBox(
            //   height: setHeight(20),
            // ),
            // Container(
            //   margin: EdgeInsets.only(
            //       left: screenWidth / 3, right: screenWidth / 3),
            //   padding: const EdgeInsets.only(
            //       left: 10, right: 10, top: 10, bottom: 10),
            //   decoration: BoxDecoration(
            //     border: Border.all(
            //       color: Colors.blue,
            //       width: 2,
            //     ),
            //     borderRadius: BorderRadius.circular(12),
            //   ),
            // child: Center(
            //   child: InkWell(
            //     child: Text(
            //       "GO HOME",
            //       style: TextStyle(
            //         color: Colors.blue[900],
            //         fontWeight: FontWeight.bold,
            //         fontSize: setFontSize(16),
            //       ),
            //     ),
            //     onTap: () {
            //       Navigator.pushNamed(
            //         context,
            //         "/HomePage",
            //       );
            //     },
            //   ),
            // ),
            // )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final tabController = DefaultTabController(
      length: daylength,
      child: Scaffold(
        appBar: AppBar(
          // automaticallyImplyLeading: false,
          toolbarHeight: isCollapsed ? 80 : 50,
          // shape: cardBorder(),
          title: const Text(
            'My Time Table',
            style: TextStyle(
              color: whiteColor,
              fontFamily: 'Roboto',
              fontSize: 20,
              fontWeight: FontWeight.w500,
            ),
          ),
          // actions: <Widget>[
          // IconButton(
          //   icon: Icon(
          //     FontAwesomeIcons.home,
          //     color: Colors.white,
          //   ),
          //   onPressed: () {
          //     // do something
          //   },
          // )
          // ],
          automaticallyImplyLeading: true,
          backgroundColor: themeColorBlue,
          bottom: TabBar(
            labelStyle: const TextStyle(fontWeight: FontWeight.w900),
            isScrollable: true,
            unselectedLabelColor: textSecondaryColor,
            indicatorColor: Colors.orange,
            labelColor: Colors.white,
            indicatorWeight: 2.0,
            indicator: BoxDecoration(
              borderRadius: BorderRadius.circular(20), // Creates border
              color: Colors.orange,
            ),
            tabs: tabNameList(),
          ),
        ),
        body: TabBarView(
          children: tabsList(),
        ),
      ),
    );

    return tabController;
  }

  Widget listofttwidget() {
    return ListView(
      children: [
        slottimetable(),
      ],
    );
  }

  Widget slottimetable() {
    return Card(
      margin: EdgeInsets.all(10.0),
      color: whiteColor,
      child: Container(
        width: screenWidth,
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Slot",
                  style: TextStyle(
                    color: textSecondaryColor,
                    fontSize: setFontSize(12),
                  ),
                ),
                Text(
                  "08:00 : 09:00",
                  style: TextStyle(
                    color: btnTextColorBlue,
                    fontSize: setFontSize(12),
                  ),
                ),
              ],
            ),
            const Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Year : Division",
                  style: TextStyle(
                    color: textSecondaryColor,
                    fontSize: setFontSize(12),
                  ),
                ),
                Text(
                  "TY : CS-A",
                  style: TextStyle(
                    color: btnTextColorBlue,
                    fontSize: setFontSize(12),
                  ),
                ),
              ],
            ),
            const Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Room No.",
                  style: TextStyle(
                    color: textSecondaryColor,
                    fontSize: setFontSize(12),
                  ),
                ),
                Text(
                  "1001",
                  style: TextStyle(
                    color: btnTextColorBlue,
                    fontSize: setFontSize(12),
                  ),
                ),
              ],
            ),
            const Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Course : Batch No.",
                  style: TextStyle(
                    color: textSecondaryColor,
                    fontSize: setFontSize(12),
                  ),
                ),
                Text(
                  "CD : B1",
                  style: TextStyle(
                    color: btnTextColorBlue,
                    fontSize: setFontSize(12),
                  ),
                ),
              ],
            ),
            const Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Load Type",
                  style: TextStyle(
                    color: textSecondaryColor,
                    fontSize: setFontSize(12),
                  ),
                ),
                Text(
                  "Lab",
                  style: TextStyle(
                    color: btnTextColorBlue,
                    fontSize: setFontSize(12),
                  ),
                ),
              ],
            ),
            const Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Online Lecture",
                  style: TextStyle(
                    color: textSecondaryColor,
                    fontSize: setFontSize(12),
                  ),
                ),
                Row(
                  textDirection: TextDirection.rtl,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    ClipOval(
                      child: Material(
                        color: errorColor, // button color
                        child: InkWell(
                          //splashColor: Colors.red, // inkwell color
                          child: SizedBox(
                            width: 25,
                            height: 25,
                            child: Icon(
                              Icons.delete,
                              color: Colors.white,
                              size: setFontSize(14),
                            ),
                          ),
                          onTap: () {},
                        ),
                      ),
                    ),
                    SizedBox(
                      width: setWidth(10),
                    ),
                    ClipOval(
                      child: Material(
                        color: Colors.lightGreen, // button color
                        child: InkWell(
                          //splashColor: Colors.red, // inkwell color
                          child: SizedBox(
                            width: 25,
                            height: 25,
                            child: Icon(
                              Icons.edit,
                              color: Colors.white,
                              size: setFontSize(14),
                            ),
                          ),
                          onTap: () {
                            setState(() {});
                          },
                        ),
                      ),
                    ),
                    SizedBox(
                      width: setWidth(10),
                    ),
                    ClipOval(
                      child: Material(
                        color: themeColorYellow, // button color
                        child: InkWell(
                          //splashColor: Colors.red, // inkwell color
                          child: SizedBox(
                            width: 25,
                            height: 25,
                            child: Icon(
                              Icons.link,
                              color: Colors.white,
                              size: setFontSize(14),
                            ),
                          ),
                          onTap: () {
                            setState(() {});
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget weekend() {
    return Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(
              FontAwesomeIcons.smileBeam,
              color: themeColorYellow,
              size: 75,
            ),
            SizedBox(
              height: setHeight(20),
            ),
            Text(
              "Weekly Day Off",
              style: TextStyle(
                fontSize: setFontSize(24),
                color: themeColorYellow,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              "",
              style: TextStyle(
                fontSize: setFontSize(24),
                color: themeColorYellow,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              "",
              style: TextStyle(
                fontSize: setFontSize(24),
                color: themeColorYellow,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }

  _validateURL(String url) async {
    bool _validURL = Uri.parse(url).isAbsolute;
    return _validURL;
  }

  _launchURL(String url) async {
    // const url = 'https://flutter.io';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

import 'dart:convert';
import 'package:epc_faculty/controller/commonapicall.dart';
import 'package:epc_faculty/pages/css/common_ui.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:epc_faculty/config/global.dart';
import 'package:shared_preferences/shared_preferences.dart';

class APICall {
  // API call for POST method with header
  static Future<String> returnAPI(String linkname, BuildContext context,
      [Map<String, dynamic>? params]) async {
    return callAPI(params!, MyApiCall.myApiCall[linkname]!, context);
  }

  static Future<String> callAPI(
      Map<String, dynamic> params, String url, BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    uid = prefs.getString("loginid")!;
    orgid = prefs.getString("orgid")!;
    var headerValue = {
      "Accept": "application/json",
      "EPC-loginid": uid,
      "router-path": "/app-link",
      "EPC-orgid": orgid,
    };
    var response;
    try {
      response = await http.post(Uri.parse(url),
          headers: headerValue, body: json.encode(params));
      var body = json.decode(response.body);
      // debugPrint(body, wrapWidth: 1024);
      final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
      pattern
          .allMatches(response.body.toString())
          .forEach((match) => print(match.group(0)));
      return response.body;
    } catch (E) {
      // toastFunctioncolor(context, "Something Went Wrong", Colors.black);
      return "Something Went Wrong";
    }
  }

  // API call for POST method without header
  static Future<String> returnPostAPIWithoutHeader(
      String linkname, BuildContext context,
      [Map<String, dynamic>? params]) async {
    print('returnPostAPIWithoutHeader');
    return callPostAPIWithoutHeader(
        params!, MyApiCall.myApiCall[linkname]!, context);
  }

  static Future<String> callPostAPIWithoutHeader(
      Map<String, dynamic> params, String url, BuildContext context) async {
    print('callPostAPIWithoutHeader');
    var headerValue = {
      "Accept": "application/json",
      "router-path": "/app-link",
    };
    var response;
    try {
      response = await http.post(Uri.parse(url),
          headers: headerValue, body: json.encode(params));
      var body = json.decode(response.body);
      // debugPrint(body, wrapWidth: 1024);
      final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
      pattern
          .allMatches(response.body.toString())
          .forEach((match) => print(match.group(0)));
      return response.body;
    } catch (E) {
      toastFunctioncolor(context, "Something Went Wrong", Colors.black);
      return "Something Went Wrong";
    }
  }

  // API call for GET method without header
  static Future<String> returnGETAPIWithoutHeader(
      String linkname, BuildContext context,
      [Map<String, dynamic>? params]) async {
    print('returnGETAPIWithoutHeader');
    return callGETAPIWithoutHeader(
        params!, MyApiCall.myApiCall[linkname]!, context);
  }

  static Future<String> callGETAPIWithoutHeader(
      Map<String, dynamic> params, String url, BuildContext context) async {
    print('callGETAPIWithoutHeader');
    if (params.isNotEmpty) {}
    var headerValue = {
      "Accept": "application/json",
      "router-path": "/app-link"
    };
    var response;
    try {
      if (params.isNotEmpty) {
        final newURI = Uri.parse(url).replace(queryParameters: params);
        response = await http.get(newURI, headers: headerValue);
        var body = json.decode(response.body);
        final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
        pattern
            .allMatches(response.body.toString())
            .forEach((match) => print(match.group(0)));
        return response.body;
      } else {
        response = await http.get(Uri.parse(url), headers: headerValue);
        var body = json.decode(response.body);
        final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
        pattern
            .allMatches(response.body.toString())
            .forEach((match) => print(match.group(0)));
        return response.body;
      }
    } catch (E) {
      toastFunctioncolor(context, "Something Went Wrong", Colors.black);
      return "Something Went Wrong";
    }
  }

  // API call for GET method with header
  static Future<String> returnAPIWithHeader(
      String linkname, BuildContext context,
      [Map<String, dynamic>? params]) async {
    return callAPIWithHeader(params!, MyApiCall.myApiCall[linkname]!, context);
  }

  static Future<String> callAPIWithHeader(
      Map<String, dynamic> params, String url, BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    uid = prefs.getString("loginid")!;
    orgid = prefs.getString("orgid")!;
    var headerValue = {
      "Accept": "application/json",
      "router-path": "/app-link",
      "EPC-loginid": uid,
      "EPC-orgid": orgid
    };
    var response;
    try {
      if (params.isNotEmpty) {
        final newURI = Uri.parse(url).replace(queryParameters: params);
        response = await http.get(newURI, headers: headerValue);
        var body = json.decode(response.body);
        final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
        pattern
            .allMatches(response.body.toString())
            .forEach((match) => print(match.group(0)));
        return response.body;
      } else {
        response = await http.get(Uri.parse(url), headers: headerValue);
        var body = json.decode(response.body);
        final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
        pattern
            .allMatches(response.body.toString())
            .forEach((match) => print(match.group(0)));
        return response.body;
      }
    } catch (E) {
      toastFunctioncolor(context, "Something Went Wrong", Colors.black);
      return "Something Went Wrong";
    }
  }
}

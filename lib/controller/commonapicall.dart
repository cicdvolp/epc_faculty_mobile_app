import 'package:epc_faculty/config/global.dart';

class MyApiCall {
  static Map<String, String> myApiCall = {
    // Login Module
    "login": url + "appLogin/login",
    "verifyemail": url + "appLogin/verifyemail",
    "verifyOTP": url + "appLogin/verifyotp",
    "resetForgotPass": url + "appLogin/savechangepassword",

    // Time Table Module
    "getTimetable": url + "apiInstructorTimeTable/getMyTimeTable",
    "sessionurl": url + "apiInstructorTimeTable/saveonlineclasslink",

    // Home Module
    "getHomeIcons": url + "apiMobileAppLink/mobileLinks",

    // Time Sheet Modele
    "getTask": url + "apiInstructorTimeSheet/getTaskList",
    "addTimesheet": url + "apiInstructorTimeSheet/addTimeSheets",
    "saveTimesheet": url + "apiInstructorTimeSheet/saveInstructorTimesheet",
    // Leaves Modele
    "getLeaves": url + "apiInstructorLeaveManagement/getLeavesDetails",
    //yash Attedance api
    "init_attedance": url + "mobileattendanceapp/init_attedance",
    "fetchemployee": url + "mobileattendanceapp/getemployeeinfo",
    "savedeviceinfo": url + "mobileattendanceapp/savemobiledeviceinfo",
    "markattendanceapi": url + "mobileattendanceapp/markMobileAppAttendance",
    "getalllocationmaster": url + "mobileattendanceapp/getalllocationmaster",
    "getlocationinformation":
        url + "mobileattendanceapp/getalllocationinformation",
    // "addregisterlocation": url + "mobileattendanceapp/registerlocationinformation",
    "addregisterlocation":
        url + "mobileattendanceapp/registerlocationmasteryash",
    "getalllocationmasterinfo":
        url + "mobileattendanceapp/getalllocationinformationbyorg",
    "deletemaster": url + "mobileattendanceapp/deletelocationmasteryash",
    "getcordinates": url + "mobileattendanceapp/getRegisteredMobileLocation",
    "getRegEmployeeList": url + "mobileattendanceapp/getRegEmployeeList",
  };
}
